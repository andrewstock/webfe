package com.detica.cyberreveal.workspace.server;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.elasticsearch.action.admin.indices.refresh.RefreshRequest;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.Client;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.detica.cyberreveal.workspace.api.DataSet;
import com.detica.cyberreveal.workspace.api.OrderSpec;
import com.detica.cyberreveal.workspace.api.Record;
import com.detica.cyberreveal.workspace.api.Workspace;
import com.github.tlrx.elasticsearch.test.EsSetup;
import com.github.tlrx.elasticsearch.test.annotations.ElasticsearchAdminClient;
import com.github.tlrx.elasticsearch.test.annotations.ElasticsearchClient;
import com.github.tlrx.elasticsearch.test.annotations.ElasticsearchNode;
import com.github.tlrx.elasticsearch.test.support.junit.runners.ElasticsearchRunner;

import static com.github.tlrx.elasticsearch.test.EsSetup.*;

@RunWith(ElasticsearchRunner.class)
@ElasticsearchNode
public class WorkspaceManagerTest {
    EsSetup esSetup;
    
    WorkspaceManager mgr;
    
    @ElasticsearchClient
    Client client;
    
    @ElasticsearchAdminClient
    AdminClient admin;
    
	@Before
	public void setUp() throws Exception {
        // Instantiates a local node & client
        esSetup = new EsSetup(client);
        // Clean all, and creates some indices
        esSetup.execute(
                deleteAll()
          //      createIndex("central")
//                createIndex("my_index_2")
//                        .withSettings(fromClassPath("path/to/settings.json"))
 //                       .withMapping("type1", fromClassPath("path/to/mapping/of/type1.json"))
  //                      .withData(fromClassPath("path/to/bulk.json")),
  //              createTemplate("template-1")
   //                     .withSource(fromClassPath("path/to/template1.json"))
        );
        
        mgr = new WorkspaceManager(client, admin, new NotificationManager());
	}

	@After
	public void tearDown() throws Exception {
        esSetup.terminate();
	}

	@Test
	public void testGetWorkspace() {
		esSetup.execute(deleteAll());
		
		Workspace ws = mgr.createWorkspaceIndex("test");
		
		Workspace ws2 = mgr.getWorkspace(ws.getId());
		
		assertEquals(ws.getName(), ws2.getName());
		//fail("Not yet implemented");
		
		esSetup.execute(deleteAll());
	}

	@Test
	public void testAddDataset() {
		esSetup.execute(deleteAll());
		
		Workspace ws = mgr.createWorkspaceIndex("test");
		
		DataSet dataset = new DataSet();
		
		mgr.addDataset(ws.getId(), dataset);
		
		refreshIndices("central", ws.getId());
		
		Workspace ws2 = mgr.getWorkspace(ws.getId());
		
		assertEquals(1, ws2.getDatasets().size());
		
		esSetup.execute(deleteIndex("central"));
		esSetup.execute(deleteAll());
	}

	@Test
	public void testAddRecord() {
	//	tested in getfullypopulateddataset
	}

	private void refreshIndices(String... indices) {
		admin.indices().refresh(new RefreshRequest(indices));
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetWorkspaces() throws InterruptedException {
		esSetup.execute(deleteAll());
		System.out.println("1");
		// Check empty to start wtih
		List<Workspace> ret = mgr.getWorkspaces();
		System.out.println("2");

		assertEquals(0, ret.size());
		System.out.println("3");
		
	Thread.sleep(2000);
		System.out.println("4");

		
		Workspace ws = mgr.createWorkspaceIndex("testx");
		
		refreshIndices("central");
		
		Thread.sleep(2000);
		
		ret = mgr.getWorkspaces();

		assertEquals(1, ret.size());

		Workspace ws2 = mgr.createWorkspaceIndex("test2");

		refreshIndices("central");
		Thread.sleep(2000);
		
		ret = mgr.getWorkspaces();

		for (Workspace w : ret) {
			System.out.println("WS: " + w.getId() + " " + w.getName());
		}
		
		assertEquals(2, ret.size());

		esSetup.execute(deleteAll());
	}

	@Test
	public void testCreateWorkspaceIndex() {
	//	Covered in get workspaces
	}

	@Test
	public void testGetDataSet() {
	//	fail("Not yet implemented");
		
		esSetup.execute(deleteAll());
		
		Workspace ws = mgr.createWorkspaceIndex("test");
		
		DataSet dataset = new DataSet();
		dataset.setTimeStarted(new Date());
		dataset.setTimeFinished(new Date());
		dataset.setHeadings(Arrays.asList("One", "Two", "Three"));
		//dataset.setDataSource("datasourcevalue");
		
		mgr.addDataset(ws.getId(), dataset);
		
		refreshIndices("central", ws.getId());
		
		Workspace ws2 = mgr.getWorkspace(ws.getId());
		
		assertEquals(1, ws2.getDatasets().size());
		
		DataSet ds = mgr.getDataSet(ws2.getId(), ws2.getDatasets().get(0).getId());
		
		assertEquals(dataset.getTimeStarted(), ds.getTimeStarted());
		assertEquals(dataset.getTimeFinished(), ds.getTimeFinished());
		assertEquals(dataset.getHeadings(),ds.getHeadings());
		
		esSetup.execute(deleteIndex("central"));
		esSetup.execute(deleteAll());
		
	}

	@Test
	public void testGetFullyPopulatedDataSet() {
	//	fail("Not yet implemented");
		esSetup.execute(deleteAll());
		
		Workspace ws = mgr.createWorkspaceIndex("test");
		
		DataSet dataset = new DataSet();
		dataset.setTimeStarted(new Date());
		dataset.setTimeFinished(new Date());
		dataset.setHeadings(Arrays.asList("One", "Two", "Three"));
		//dataset.setDataSource("datasourcevalue");
		
		DataSet added = mgr.addDataset(ws.getId(), dataset);
		
		Record record = new Record(Arrays.asList((Object)"One", "Two", "Three"));
		mgr.addRecord(ws.getId(), added.getId(), record);
		
		refreshIndices("central", ws.getId());
		
		Workspace ws2 = mgr.getWorkspace(ws.getId());
		
		assertEquals(1, ws2.getDatasets().size());
		
		DataSet ds = mgr.getFullyPopulatedDataSet(ws.getId(), added.getId(), 0, 10, null, new ArrayList<OrderSpec>());
		
		List<Record> records = ds.getRecords();
		
		assertEquals(1, records.size());
		assertEquals("One", records.get(0).getData().get(0));
	
		assertEquals(1, ds.getRecordsTotal());
		assertEquals(1, ds.getRecordsFiltered());
		
		
		esSetup.execute(deleteIndex("central"));
		esSetup.execute(deleteAll());
	}

}
