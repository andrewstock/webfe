package com.detica.cyberreveal.alerts.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.detica.cyberreveal.alerts.api.AlertBean;
import com.detica.cyberreveal.alerts.api.AlertBeanHierarchy;

@Controller
public class AlertController {
	  @RequestMapping(value = "/alerts", method = RequestMethod.GET)
	  public @ResponseBody
	  List<AlertBean> getAlerts() {
		  List<AlertBean> ret = new ArrayList<AlertBean>();
		  
		  AlertBean alert = new AlertBean();
		  alert.setAssignee("astock");
		  alert.setId(1l);
		  AlertBeanHierarchy h = new AlertBeanHierarchy();
		  h.setPath(new String[] {"One", "Two"});
		  
		  alert.setHierarchyList(Arrays.asList(h));
		  
		  ret.add(alert);
		  
	      return ret;
	  }
}
