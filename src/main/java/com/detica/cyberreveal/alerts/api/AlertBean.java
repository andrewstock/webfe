package com.detica.cyberreveal.alerts.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class AlertBean {
    private Long id;
    private int priority;
    private String state;
    private String organisation;
    private String source;
    private String type;
    private String subType;
    private String assignee;
    private Long creationDate;
    private Long startDate;
    private Long endDate;
    private String transactionId;
    private Set<AlertBeanProperty> properties = new LinkedHashSet<AlertBeanProperty>();
    private List<AlertBeanHierarchy> hierarchyList = new ArrayList<AlertBeanHierarchy>();
    private String explanation;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getOrganisation() {
		return organisation;
	}
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getAssignee() {
		return assignee;
	}
	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}
	public Long getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Long creationDate) {
		this.creationDate = creationDate;
	}
	public Long getStartDate() {
		return startDate;
	}
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	public Long getEndDate() {
		return endDate;
	}
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public Set<AlertBeanProperty> getProperties() {
		return properties;
	}
	public void setProperties(Set<AlertBeanProperty> properties) {
		this.properties = properties;
	}
	public List<AlertBeanHierarchy> getHierarchyList() {
		return hierarchyList;
	}
	public void setHierarchyList(List<AlertBeanHierarchy> hierarchyList) {
		this.hierarchyList = hierarchyList;
	}
	public String getExplanation() {
		return explanation;
	}
	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}
    
    
}
