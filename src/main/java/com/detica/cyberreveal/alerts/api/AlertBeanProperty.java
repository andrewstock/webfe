package com.detica.cyberreveal.alerts.api;

import java.util.HashSet;
import java.util.Set;

public class AlertBeanProperty {
	 private String name;
	    private String value;
	    private String type = "string";
	    private int additionalFlags = 1; // binary value representing isVisible (1)
	                                     // and not isAdditional (2)
	    // private boolean isVisible = true;
	    // private boolean isAdditional = false;
	    private Set<String> relatedProperties = new HashSet<String>(0);
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
		public String getType() {
			return type;
		}
		public void setType(String type) {
			this.type = type;
		}
		public int getAdditionalFlags() {
			return additionalFlags;
		}
		public void setAdditionalFlags(int additionalFlags) {
			this.additionalFlags = additionalFlags;
		}
		public Set<String> getRelatedProperties() {
			return relatedProperties;
		}
		public void setRelatedProperties(Set<String> relatedProperties) {
			this.relatedProperties = relatedProperties;
		}
	    
	    
}
