package com.detica.cyberreveal.security;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;

import com.detica.cyberreveal.audit.AuditClient;
import com.detica.cyberreveal.audit.IAuditClient;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;

public class TokenUtils {
	
	private static final String SECRET = "onO9Ihudz3WkiauDO2Uhyuz0Y18UASXlSc1eS0NkWyA";
	private static final Map<String, UserDetails> credentials = new HashMap<String, UserDetails>();
	private static final String LOG_VISIBILITY_EXPRESSION = "ADMIN";
	
	@Autowired
	private IAuditClient auditClient;
	
	 String getToken(UserDetails userDetails) {
		 // Need to check this is OK!
			// Check username and password
			boolean authenticated = "andrew".equals(userDetails.getUsername())
					&& "password".equals(userDetails.getPassword());

			if (!authenticated) {
				auditClient.logEntry("userAccess", userDetails.getUsername(), "Failed authentication attempt", Arrays.asList("system"), LOG_VISIBILITY_EXPRESSION);
				throw new AuthenticationFailedException();
			}
		
			// otherwise issue token
			auditClient.logEntry("userAccess", userDetails.getUsername(), "Successful authentication attempt", Arrays.asList("system"), LOG_VISIBILITY_EXPRESSION);
			
			// Other parts of the system will require these credentials to log in to downstream systems
			
			credentials.put(userDetails.getUsername(), userDetails);

		 return generateToken(userDetails.getUsername());
	 }

	 boolean validate(String token) {
	    return verifyToken(token) != null;
	    
	 }
	    
	 UserDetails getUserFromToken(String token) {
	    if (validate(token)) {
	    	return credentials.get(verifyToken(token));
	    }
	    return null;
	 }
	 
	 public String verifyToken(String token) {

			SignedJWT signedJWT;
			try {
				signedJWT = SignedJWT.parse(token);

			JWSVerifier verifier = new MACVerifier(SECRET);

				if (!signedJWT.verify(verifier)) {
					return null;
				}
				// TODO: add checks for time etc

				return signedJWT.getJWTClaimsSet().getSubject();
			} catch (ParseException e1) {
				e1.printStackTrace();
				return null;
			} catch (JOSEException e) {
				e.printStackTrace();
				return null;
			}
		}

		private String generateToken(String username) {
			// Create HMAC signer
			JWSSigner signer = new MACSigner(SECRET);

			// Prepare JWT with claims set
			JWTClaimsSet claimsSet = new JWTClaimsSet();
			claimsSet.setSubject(username);
			claimsSet.setIssueTime(new Date());
			claimsSet.setIssuer("cyberreveal");

			SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256),
					claimsSet);

			// Apply the HMAC
			try {
				signedJWT.sign(signer);
				// To serialize to compact form, produces something like
				// eyJhbGciOiJIUzI1NiJ9.SGVsbG8sIHdvcmxkIQ.onO9Ihudz3WkiauDO2Uhyuz0Y18UASXlSc1eS0NkWyA
				return signedJWT.serialize();
			} catch (JOSEException e) {
				e.printStackTrace();
			}

			return null;
		}
}
