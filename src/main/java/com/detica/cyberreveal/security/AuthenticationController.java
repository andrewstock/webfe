package com.detica.cyberreveal.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class AuthenticationController {
	
	@Autowired
	private TokenUtils tokenUtils;

	@RequestMapping(value="/whoami", method=RequestMethod.GET)
	public @ResponseBody String whoAmI() {
		return "hello";
	}
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public @ResponseBody
	AuthToken authenticateUser(@RequestBody AuthRequest ar) {
		AuthToken token = new AuthToken();

		CyberRevealUser user = new CyberRevealUser(ar.getUsername(), ar.getPassword(), new ArrayList<GrantedAuthority>());
		
		String jwToken = tokenUtils.getToken(user);
		
		token.setWebToken(jwToken);
		
		return token;
	}

	
}
