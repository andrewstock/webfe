package com.detica.cyberreveal.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.GenericFilterBean;

/**
 * Filter which will check for a valid JSON web token in the request and if it exists, set the request as authenticated. 
 * @author astock
 *
 */
public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

    @Autowired TokenUtils tokenUtils;

    public AuthenticationTokenProcessingFilter() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        @SuppressWarnings("unchecked")
        Map<String, String[]> parms = request.getParameterMap();

        HttpServletRequest req = (HttpServletRequest)request;
        String header = req.getHeader("authorization");
        
        String token = null;
        
        // This allows the token to either come in via a header or via a parameter 
        if (header != null) {
        	header = header.trim();
        	
        	if (header.toLowerCase().startsWith("bearer")) {
        		// Looks possible
                // Need to strip off Bearer from start
        		token = header.substring(6).trim();
        	}
        } else {
            if(parms.containsKey("_token")) {
                token = parms.get("_token")[0]; // grab the first "token" parameter
            }
        }
                
        if(token != null) {
            // validate the token
            if (tokenUtils.validate(token)) {
                // determine the user based on the (already validated) token
                UserDetails userDetails = tokenUtils.getUserFromToken(token);
                
                if (userDetails != null) {
                	// If null, the server doesn't hold the details in memory and the user will need to reauthenticate
                	
	                // build an Authentication object with the user's info
	                UsernamePasswordAuthenticationToken authentication = 
	                        new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
	                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails((HttpServletRequest) request));

	                // set the authentication into the SecurityContext
	                UsernamePasswordAuthenticationToken upa = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword(), (Collection<? extends GrantedAuthority>) new ArrayList<GrantedAuthority>());
	                upa.setDetails(userDetails);
	                SecurityContextHolder.getContext().setAuthentication(upa);
                }
            }
        }
        // continue through the filter chain
        chain.doFilter(request, response);
    }
}