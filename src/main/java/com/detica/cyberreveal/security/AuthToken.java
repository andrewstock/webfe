package com.detica.cyberreveal.security;

public class AuthToken {
	private String webToken;

	public String getWebToken() {
		return webToken;
	}

	public void setWebToken(String webToken) {
		this.webToken = webToken;
	}
}
