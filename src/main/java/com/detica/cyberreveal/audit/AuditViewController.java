package com.detica.cyberreveal.audit;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.detica.cyberreveal.workspace.api.Attribute;
import com.detica.cyberreveal.workspace.api.DataSet;
import com.detica.cyberreveal.workspace.api.EntitySpec;
import com.detica.cyberreveal.workspace.api.OrderSpec;
import com.detica.cyberreveal.workspace.api.Projection;
import com.detica.cyberreveal.workspace.api.Record;
import com.detica.cyberreveal.workspace.api.RelationshipSpec;
import com.detica.cyberreveal.workspace.api.Workspace;
import com.detica.cyberreveal.workspace.server.AbstractDocumentController;
import com.detica.cyberreveal.workspace.server.WorkspaceManager;

/**
 * Created with IntelliJ IDEA. User: cpa199 Date: 26/10/13 Time: 18:04 To change
 * this template use File | Settings | File Templates.
 */
@Controller
public class AuditViewController extends AbstractDocumentController implements InitializingBean {

	private static final String LOG_ENTRY = "LogEntry";
	private static final String TABLE_NAME = "audit";
	
	@Autowired
	private Client client;
	private final ObjectMapper mapper = new ObjectMapper(); // create once, reuse
	private AdminClient clientAdmin;

	public AuditViewController() {

//		if (client == null) {
//			ImmutableSettings.Builder settings = ImmutableSettings
//					.settingsBuilder();
//
//			//settings.put("node.name", "orange11-node");
//
//			//settings.put("path.data", "C:/devtools/index");
//
//			settings.put("http.enabled", true);
////
//			Node node = NodeBuilder.nodeBuilder()
//
//			.settings(settings)
//
//			.clusterName("orange11-cluster")
//
//			.data(false).local(false).node();
//
//			client = node.client();
//			
////			 Settings s = ImmutableSettings.settingsBuilder().put("cluster.name", 
////					 "orange11-cluster").build(); 
////					 TransportClient client = new TransportClient(s); 
////					 client.addTransportAddress(new InetSocketTransportAddress("localhost", 9200)); 
////					 this.client = client;
//			
//			clientAdmin = client.admin();
//			
//			// Check that index exists
//			checkAuditIndexExists();
//		}
	}


	@RequestMapping(value = "/audit/{log}/", method = RequestMethod.GET)
	public @ResponseBody AuditData getDataSet(@PathVariable String log,
		//	@PathVariable String datasetId,
			@RequestParam(value = "draw", defaultValue = "0") int draw,
			@RequestParam(value = "start", defaultValue = "0") int start,
			@RequestParam(value = "length", defaultValue = "10") int length,
			@RequestParam MultiValueMap<String, String> otherParams) {

		String searchText = null;
		List<String> searchValues = otherParams.get("search[value]");
		if (searchValues != null && searchValues.size() > 0) {
			// User is searching
			searchText = searchValues.get(0);
		}
		
		List<OrderSpec> ordering = new ArrayList<OrderSpec>();
		String lastColumn = "";
		
		AuditData ds = new AuditData();
		ds.setDraw(draw);

		QueryBuilder builder = QueryBuilders.matchAllQuery();
		BoolQueryBuilder bqb = QueryBuilders.boolQuery();
		if (otherParams.containsKey("user")) {
			builder = QueryBuilders.termQuery("user", otherParams.getFirst("user"));
			bqb.must(builder);
		}

		if (otherParams.containsKey("action")) {
			builder = QueryBuilders.termQuery("action", otherParams.getFirst("action"));
			bqb.must(builder);
		}

		if (otherParams.containsKey("target")) {
			builder = QueryBuilders.termQuery("target", otherParams.getFirst("target"));
			bqb.must(builder);
		}
		
		SearchRequestBuilder search = client.prepareSearch(TABLE_NAME)
				.setTypes(LOG_ENTRY)
				.setQuery(bqb.hasClauses() ? bqb : QueryBuilders.matchAllQuery())//QueryBuilders.termQuery("log", log))//new MatchAllQueryBuilder())
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
				.setPostFilter(FilterBuilders.termFilter("log", log.toLowerCase())) // Filter
				
				.setFrom(start).setSize(length);

		if (searchText != null) {
			// User is searching
			search.setQuery(QueryBuilders.prefixQuery("data",
					searchText));
		}

		for (String key : otherParams.keySet()) {
			
			if (key.startsWith("order[")) {
				// This is ordering information
				if (key.contains("column")) {
					lastColumn = otherParams.getFirst(key);
				} else {
					OrderSpec os = new OrderSpec(lastColumn, otherParams.getFirst(key));
					ordering.add(os);
					SortBuilder sb = SortBuilders.fieldSort(lastColumn);
					if ("desc".equals(otherParams.getFirst(key))) {
						sb.order(SortOrder.DESC);
					} else {
						sb.order(SortOrder.ASC);
					}
					search.addSort(sb);
				}
			}
		}

		SearchResponse response = search.execute().actionGet();

		ds.setRecordsTotal(response.getHits().getTotalHits());
		List<LogEntry> entries = new ArrayList<LogEntry>();
		for (SearchHit hit : response.getHits()) {
			LogEntry le = deserialize(hit.getSourceAsString(), LogEntry.class);
			//le.setId(hit.getId());
			entries.add(le);
		}
		ds.setEntries(entries);
		return ds;
	}
	
	private <T> T deserialize(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			T obj = mapper.readValue(json, clazz);
		
			return obj;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public void createAuditIndex() {
		
		// Create the index
		createIndex(TABLE_NAME);
		
		// Create the mappings
		createMapping(TABLE_NAME, LOG_ENTRY, "/indices/audit-mapping.json");

	}
	
	private void createIndex(String indexName) {
		CreateIndexRequestBuilder cirb = clientAdmin.indices().prepareCreate(indexName);
		CreateIndexResponse createIndexResponse = cirb.execute().actionGet();
	}

	private void checkAuditIndexExists() {
		if (!client.admin().indices().prepareExists(TABLE_NAME).execute().actionGet().isExists()) {
			// Need to create
			System.err.println("Creating central index");
			
			createIndex(TABLE_NAME);
			
			// Optionally now add mappings
		}
	}

	private void createMapping(String indexName, String type, String mappingSourceFile) {
		PutMappingRequestBuilder pmrb = clientAdmin.indices()
				.preparePutMapping(indexName)
				.setType(type);

		InputStream is = this.getClass().getResourceAsStream(mappingSourceFile);
		try {
			String mappingSource = IOUtils.toString(is);
			
			//System.err.println(mappingSource);
			
			pmrb.setSource(mappingSource);
			
			// Create type and mapping
			PutMappingResponse response = pmrb.execute().actionGet();	

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	private String convert(Object object) {
		

		// generate json
		
		try {
			return mapper.writeValueAsString(object);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	@Override
	public void afterPropertiesSet() throws Exception {
		clientAdmin = client.admin();
		checkAuditIndexExists();
	}
}
