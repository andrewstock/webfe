package com.detica.cyberreveal.audit;

import java.util.List;

public class AuditData {
	/**
	 * Value used to help compatibility with datatables
	 */
	private int draw;
	
	/**
	 * Used when supplying this back via API. The total number of records in the dataset.
	 */
	private long recordsTotal;
		
	/**
	 * The {@link LogEntry} entries.
	 */
	private List<LogEntry> entries;

	public int getDraw() {
		return draw;
	}

	public void setDraw(int draw) {
		this.draw = draw;
	}

	public long getRecordsTotal() {
		return recordsTotal;
	}

	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}

	public List<LogEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<LogEntry> entries) {
		this.entries = entries;
	}
	
	
}
