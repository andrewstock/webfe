package com.detica.cyberreveal.workspace.api;

import java.util.List;

public class Graphlet {

	private List<Entity> entities;
	private List<Relationship> relationships;
	
	public List<Entity> getEntities() {
		return entities;
	}
	public void setEntities(List<Entity> entities) {
		this.entities = entities;
	}
	public List<Relationship> getRelationships() {
		return relationships;
	}
	public void setRelationships(List<Relationship> relationships) {
		this.relationships = relationships;
	}
	
	
}
