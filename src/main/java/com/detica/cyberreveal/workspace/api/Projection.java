package com.detica.cyberreveal.workspace.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Defines a projection between a set of returned data results and a graph representation of that view
 *
 */
public class Projection {
	/**
	 * The specifications used to build the {@link Entity} objects.
	 */
	private List<EntitySpec> entities = new ArrayList<EntitySpec>();
	
	/**
	 * The specifications used to build the {@link Relationship} objects.
	 */
	private List<RelationshipSpec> relationships = new ArrayList<RelationshipSpec>();
	
	/**
	 * The list of {@link MappingSet} options available for this projection.
	 */
	private List<MappingSet> mappingSets = new ArrayList<MappingSet>();
	
	public List<MappingSet> getMappingSets() {
		return mappingSets;
	}

	public void setMappingSets(List<MappingSet> mappings) {
		this.mappingSets = mappings;
	}
	
	public void addMappingSet(MappingSet mapping) {
		this.mappingSets.add(mapping);
	}
	public void addEntity(EntitySpec spec) {
		this.entities.add(spec);
	}
	
	public void addRelationship(RelationshipSpec spec) {
		this.relationships.add(spec);
	}

	
	public List<EntitySpec> getEntities() {
		return entities;
	}
	public void setEntities(List<EntitySpec> entities) {
		this.entities = entities;
	}
	public List<RelationshipSpec> getRelationships() {
		return relationships;
	}
	public void setRelationships(List<RelationshipSpec> relationships) {
		this.relationships = relationships;
	}
	
	
}
