package com.detica.cyberreveal.workspace.api;

/**
 * Represents an attribute of an entity or relationsihp
 *
 */
public class Attribute {
	/**
	 * The name of the attribute
	 */
	private String name;
	
	/**
	 * The value of the attribute
	 */
	private Object value;
	
	/**
	 * The security label assocated with this attribute
	 */
	private String securityLabel;
	
	public Attribute() {
		
	}
	
	public Attribute(String name, Object value) {
		this.name = name;
		this.value = value;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Object getValue() {
		return value;
	}
	public void setValue(Object value) {
		this.value = value;
	}

	public String getSecurityLabel() {
		return securityLabel;
	}

	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}
	
	
}
