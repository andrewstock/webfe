package com.detica.cyberreveal.workspace.api;

import java.util.List;

/**
 * Represents a single record returned from a query. 
 *
 */
public class Record {
	private List<Object> data;
	
	/**
	 * Labels for the data. If this list has only a single item, then that is considered to apply to the whole record.
	 * 
	 * Alternatively, it needs to contain a label for each record
	 */
	private List<String> securityLabels;
	
	/**
	 * The data set ID that this record is part of
	 */
	private String dataset;
	
	/**
	 * Holds the source of this record. Only used where results are federated from a number of sources
	 * and something is required to differentiate .
	 */
	private String source;
	
	public Record() {
		
	}
	
	public Record(List<Object> data) {
		setData(data);
	}
	
	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public List<Object> getData() {
		return data;
	}

	public void setData(List<Object> data) {
		this.data = data;
	}

	public List<String> getSecurityLabels() {
		return securityLabels;
	}

	public void setSecurityLabels(List<String> securityLabels) {
		this.securityLabels = securityLabels;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
	
}
