package com.detica.cyberreveal.workspace.api;

/**
 * The specification of an order
 *
 */
public class OrderSpec {
	/**
	 * The column to sort on
	 */
	private String column;
	
	/**
	 * The direction of the sort applied to the column.
	 */
	private String direction;
	
	public OrderSpec() {
		
	}
	
	public OrderSpec(String column, String direction) {
		this.column = column;
		this.direction = direction;
	}
	
	public String getColumn() {
		return column;
	}
	
	public void setColumn(String column) {
		this.column = column;
	}
	
	public String getDirection() {
		return direction;
	}
	
	public void setDirection(String direction) {
		this.direction = direction;
	}
}
