package com.detica.cyberreveal.workspace.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The definition of a dataset as returned from a query.
 *
 */
public class DataSet {
	
	/**
	 * The ID of this dataset in the index.
	 */
	private String id;
	
	/**
	 * The list of {@link Record} objects that make up this dataset.
	 */
	private List<Record> records;
	
	/**
	 * When this dataset creation was initiated.
	 */
	private Date timeStarted;
	
	/**
	 * When the creation of the dataset was completed (i.e. all query results were in)
	 */
	private Date timeFinished;
	
	/**
	 * Tags associated with the dataset
	 */
	private List<String> tags;
	
	/**
	 * The headings for the fields in the records.
	 */
	private List<String> headings;
	
	/**
	 * Value used to help compatibility with datatables
	 */
	private int draw;
	
	/**
	 * Used when supplying this back via API. The total number of records in the dataset.
	 */
	private long recordsTotal;
	
	/**
	 * When a filter has been applied, the number of records that passed the filter.
	 */
	private long recordsFiltered;
	
	/**
	 * The list of projections available for this dataset
	 */
	private List<Projection> projections = new ArrayList<Projection>();
	
	/**
	 * The query used to create this dataset originally.
	 */
	private Query originatingQuery;
	
	/**
	 * The security label associated with this dataset, determining its overriding visibility
	 */
	private String securityLabel;
	
	public void addProjection(Projection projection) {
		this.projections.add(projection);
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public List<Record> getRecords() {
		return records;
	}
	public void setRecords(List<Record> records) {
		this.records = records;
	}
	public int getDraw() {
		return draw;
	}
	public void setDraw(int draw) {
		this.draw = draw;
	}
	public long getRecordsTotal() {
		return recordsTotal;
	}
	public void setRecordsTotal(long recordsTotal) {
		this.recordsTotal = recordsTotal;
	}
	public long getRecordsFiltered() {
		return recordsFiltered;
	}
	public void setRecordsFiltered(long recordsFiltered) {
		this.recordsFiltered = recordsFiltered;
	}
	public List<Projection> getProjections() {
		return projections;
	}
	public void setProjections(List<Projection> projections) {
		this.projections = projections;
	}
	public List<String> getHeadings() {
		return headings;
	}
	public void setHeadings(List<String> headings) {
		this.headings = headings;
	}
	public Date getTimeStarted() {
		return timeStarted;
	}
	public void setTimeStarted(Date timeStarted) {
		this.timeStarted = timeStarted;
	}
	public Date getTimeFinished() {
		return timeFinished;
	}
	public void setTimeFinished(Date timeFinished) {
		this.timeFinished = timeFinished;
	}
	public List<String> getTags() {
		return tags;
	}
	public void setTags(List<String> tags) {
		this.tags = tags;
	}
	public Query getOriginatingQuery() {
		return originatingQuery;
	}
	public void setOriginatingQuery(Query originatingQuery) {
		this.originatingQuery = originatingQuery;
	}
	public String getSecurityLabel() {
		return securityLabel;
	}
	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}
}
