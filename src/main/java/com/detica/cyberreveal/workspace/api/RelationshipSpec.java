package com.detica.cyberreveal.workspace.api;

/**
 * The specification of how to build a {@link Relationship}.
 *
 */
public class RelationshipSpec extends Relationship {
	
	/**
	 * The specification of the source entity.
	 */
	private String source;
	
	/**
	 * The specification of the dest entity.
	 */
	private String dest;
	
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	
	
}
