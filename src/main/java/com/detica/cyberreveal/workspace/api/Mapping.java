package com.detica.cyberreveal.workspace.api;

/**
 * Used to map a graph into a table. 
 *
 */
public class Mapping {
	/**
	 * Name of the column
	 */
	private String name;
	
	/**
	 * Specification string for the column.
	 * 
	 * Format: [<entity type>]\[<role>\][.<attribute>[|<entity type cast>]]
	 * 
	 * Where <entity type> is the originating entity type (only used if the source is an entity
	 * <role> is the role of the entity or relationship
	 * <attribute> is the optional attribute of the entity or relationship
	 * <entity type cast> is the resulting required entity type - allows an attribute to be represented as an entity
	 */
	private String value;
	
	public Mapping() {
		
	}
	
	public Mapping(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
