package com.detica.cyberreveal.workspace.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Relationship {
	private String id;
	private String sourceId;
	private String destId;
	private String type;
	private String role;
	private List<Attribute> attributes = new ArrayList<Attribute>();
	private String dataPermissions;
	private Date startDateTime;
	private Date endDataTime;
	private String securityLabel;
	
	
	public String getDataPermissions() {
		return dataPermissions;
	}
	public void setDataPermissions(String dataPermissions) {
		this.dataPermissions = dataPermissions;
	}
	public Date getStartDateTime() {
		return startDateTime;
	}
	public void setStartDateTime(Date startDateTime) {
		this.startDateTime = startDateTime;
	}
	public Date getEndDataTime() {
		return endDataTime;
	}
	public void setEndDataTime(Date endDataTime) {
		this.endDataTime = endDataTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}
	public String getDestId() {
		return destId;
	}
	public void setDestId(String destId) {
		this.destId = destId;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}

	public void addAttribute(Attribute attribute) {
		this.attributes.add(attribute);
	}
	public String getSecurityLabel() {
		return securityLabel;
	}
	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}
	
	
}
