package com.detica.cyberreveal.workspace.api;

import java.util.ArrayList;
import java.util.List;

public class Entity {
	private String id = "";
	private String type = "";
	private String value = "";
	private String role = "";
	private List<Attribute> attributes = new ArrayList<Attribute>();
	private String dataPermissions;
	private String securityLabel;
	
	public String getDataPermissions() {
		return dataPermissions;
	}
	public void setDataPermissions(String dataPermissions) {
		this.dataPermissions = dataPermissions;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public List<Attribute> getAttributes() {
		return attributes;
	}
	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	public void addAttribute(Attribute attribute) {
		this.attributes.add(attribute);
	}
	public String getSecurityLabel() {
		return securityLabel;
	}
	public void setSecurityLabel(String securityLabel) {
		this.securityLabel = securityLabel;
	}

	
}
