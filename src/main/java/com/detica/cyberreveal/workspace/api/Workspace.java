package com.detica.cyberreveal.workspace.api;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Represents a workspace; a collection of data related to a single topic
 *
 */
public class Workspace {
	/**
	 * Name of the workspace
	 */
	private String name = "";
	
	/**
	 * The type of the workspaces. Allows differentiation between, for example, actor group, incident, event
	 */
	private String type = "default";
	
	/**
	 * Assigned ID for this workspace
	 */
	private String id = "";
	
	/**
	 * Date this workspace was created.
	 */
	private Date created = new Date();
	
	/**
	 * All files in this workspace
	 */
	private List<String> files = new ArrayList<String>();
	
	private List<String> graphs = new ArrayList<String>();
	
	/**
	 * The {@link DataSet} objects that are part of this workspace.
	 */
	private List<DataSet> datasets = new ArrayList<DataSet>();
	private String readDataPermissions;
	private String writeDataPermissions;

	public String getReadDataPermissions() {
		return readDataPermissions;
	}

	public void setReadDataPermissions(String readDataPermissions) {
		this.readDataPermissions = readDataPermissions;
	}

	public String getWriteDataPermissions() {
		return writeDataPermissions;
	}

	public void setWriteDataPermissions(String writeDataPermissions) {
		this.writeDataPermissions = writeDataPermissions;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public List<String> getFiles() {
		return files;
	}

	public void setFiles(List<String> files) {
		this.files = files;
	}

	public List<String> getGraphs() {
		return graphs;
	}

	public void setGraphs(List<String> graphs) {
		this.graphs = graphs;
	}

	public List<DataSet> getDatasets() {
		return datasets;
	}

	public void setDatasets(List<DataSet> datasets) {
		this.datasets = datasets;
	}

	public Workspace() {
		
	}
	
	public Workspace(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Workspace[name=" + name + ",id=" + id + "]";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	
}
