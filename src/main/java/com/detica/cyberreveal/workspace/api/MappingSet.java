package com.detica.cyberreveal.workspace.api;

import java.util.ArrayList;
import java.util.List;

/**
 * Provides a complete set of {@link Mapping} objects. 
 *
 */
public class MappingSet {
	
	/**
	 * The list of {@link Mapping} objects
	 */
	private List<Mapping> mappings = new ArrayList<Mapping>();
	
	/**
	 * The name of this mapping set
	 */
	private String name;
	
	/**
	 * The description of this mapping set
	 */
	private String description;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Mapping> getMappings() {
		return mappings;
	}

	public void setMappings(List<Mapping> mappings) {
		this.mappings = mappings;
	}
	
	public void addMapping(Mapping mapping) {
		this.mappings.add(mapping);
	}
}
