package com.detica.cyberreveal.workspace.api;

import java.util.List;

public class Graph {
	private List<Graphlet> graphlets;

	public List<Graphlet> getGraphlets() {
		return graphlets;
	}

	public void setGraphlets(List<Graphlet> graphlets) {
		this.graphlets = graphlets;
	}
	
	
}
