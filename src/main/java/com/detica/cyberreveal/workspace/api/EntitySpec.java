package com.detica.cyberreveal.workspace.api;

/**
 * The specification of how to build a {@link Entity}.
 *
 */
public class EntitySpec extends Entity {
	/**
	 * The specification of how to build the entity
	 */
	private String source;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	
}
