package com.detica.cyberreveal.workspace.server;

import org.elasticsearch.index.engine.VersionConflictEngineException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;

/**
 * Created with IntelliJ IDEA.
 * User: cpa199
 * Date: 26/10/13
 * Time: 18:09
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDocumentController {

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public String handleGenericException(Throwable e, HttpServletResponse rsp)
    {
        e.printStackTrace();
        return e.getLocalizedMessage();
    }

    @ExceptionHandler(VersionConflictEngineException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    @ResponseBody
    public String handleVersionException(Throwable e, HttpServletResponse rsp)
    {
        e.printStackTrace();
        return e.getLocalizedMessage();
    }
}
