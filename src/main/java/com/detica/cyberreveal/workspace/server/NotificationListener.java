package com.detica.cyberreveal.workspace.server;

public interface NotificationListener {
	void messagedReceived(String messageType, Object object);
}
