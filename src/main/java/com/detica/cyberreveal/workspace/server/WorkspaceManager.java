package com.detica.cyberreveal.workspace.server;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingRequestBuilder;
import org.elasticsearch.action.admin.indices.mapping.put.PutMappingResponse;
import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.AdminClient;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.script.AbstractExecutableScript;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;

import com.detica.cyberreveal.workspace.api.Attribute;
import com.detica.cyberreveal.workspace.api.DataSet;
import com.detica.cyberreveal.workspace.api.EntitySpec;
import com.detica.cyberreveal.workspace.api.Mapping;
import com.detica.cyberreveal.workspace.api.MappingSet;
import com.detica.cyberreveal.workspace.api.OrderSpec;
import com.detica.cyberreveal.workspace.api.Projection;
import com.detica.cyberreveal.workspace.api.Query;
import com.detica.cyberreveal.workspace.api.Record;
import com.detica.cyberreveal.workspace.api.RelationshipSpec;
import com.detica.cyberreveal.workspace.api.Workspace;

public class WorkspaceManager {
	
	private static final String WORKSPACE_TYPE = "workspace";
	static final String DATASET_TYPE = "dataset";
	static final String RECORD_TYPE = "record";
	private final Client client;
	private final static String CENTRAL_INDEX = "central";
	private final ObjectMapper mapper = new ObjectMapper(); // create once, reuse
	private AdminClient clientAdmin;
	private NotificationManager notificationManager;
	
	public WorkspaceManager(Client client, NotificationManager notificationManager) {
		this(client, client.admin(), notificationManager);
	}
	
	public WorkspaceManager(Client client, AdminClient clientAdmin, NotificationManager notificationManager) {
		this.client = client;
		this.clientAdmin = clientAdmin;
		this.notificationManager = notificationManager;
	}
	
	public static void main(String[] args) {
		 ImmutableSettings.Builder settings = ImmutableSettings.settingsBuilder();
		  
//	      settings.put("node.name", "orange11-node");
//	  
//	      settings.put("path.data", "C:/devtools/index");
//	  
//	      settings.put("http.enabled", true);
	  
	      //Node node = NodeBuilder.nodeBuilder()
	  
	       //   .settings(settings)
	  
	         // .clusterName("orange11-cluster")
	  
	       //   .data(false).local(false)
	     //     .node();
	      
	      
	      
	   //   Client client = node.client();
	      
	      Node node = NodeBuilder.nodeBuilder().clusterName("orange11-cluster").node();
	     Client client = node.client();
//		 Settings s = ImmutableSettings.settingsBuilder().put("cluster.name", 
//				 "orange11-cluster").build(); 
//				 TransportClient client = new TransportClient(s); 
//				 client.addTransportAddress(new InetSocketTransportAddress("localhost", 9200)); 
//				 //return client; 
		 
	      
	      WorkspaceManager mgr = new WorkspaceManager(client, null);
	      
	     // mgr.createWorkspaceIndex("WS1");
	     
	      CountResponse countResponse = client.prepareCount("20140717184336")
				//	.setTypes(WorkspaceManager.RECORD_TYPE)
					//.setQuery(QueryBuilders.matchAllQuery())
					.setQuery(QueryBuilders.termQuery("dataset", "s7nl-ybVRdmVn3kC-Z5l0w"))
//					                                              kzQK0EwySwWLVMu_2e0r3Q
					.execute().actionGet();
	      System.out.println(countResponse.getCount());
	      
	      System.exit(0);
	      //mgr.createWorkspaceIndex("My workspace");
	      
	      //client.admin().indices().prepareRefresh("_all").execute().actionGet();
	      
	   //   List<Workspace> workspaces = mgr.getWorkspaces();
	      
	      DataSet ds = new DataSet();
	      
			Projection proj = new Projection();
			EntitySpec e1 = new EntitySpec();
			e1.setType("ipaddress");
			e1.setRole("source");
			e1.setValue("<srcip>");
			e1.addAttribute(new Attribute("hostname", "<srchost>"));

			EntitySpec e2 = new EntitySpec();
			e2.setType("ipaddress");
			e2.setRole("dest");
			e2.setValue("<destip>");
			e2.addAttribute(new Attribute("hostname", "<desthost>"));

			proj.addEntity(e1);
			proj.addEntity(e2);

			RelationshipSpec r1 = new RelationshipSpec();
			r1.setSource("ipaddress[source]");
			r1.setDest("ipaddress[dest]");
			r1.setType("comms.http");
			r1.setRole("http");
			r1.addAttribute(new Attribute("size", "<size>"));
			proj.addRelationship(r1);
			ds.addProjection(proj);

			ds.setHeadings(Arrays.asList("srcip", "destip", "srchost", "desthost",
					"size"));
			
			MappingSet ms = new MappingSet();

			ms.addMapping(new Mapping("Source host", "ipaddress[source].hostname|hostname"));
			ms.addMapping(new Mapping("Source IP", "ipaddress[source]"));
			ms.addMapping(new Mapping("Destination host", "ipaddress[dest].hostname|hostname"));
			ms.addMapping(new Mapping("Destination IP", "ipaddress[dest]"));
			ms.addMapping(new Mapping("Size", "[http].size"));
			
			proj.addMappingSet(ms);
			
			String workspaceId = "20140717184336";
			
			ds = mgr.addDataset(workspaceId, ds);
			
			mgr.addRecord(workspaceId, ds.getId(), new Record(Arrays.asList((Object)"1.1.1.1", "10.1.1.1", "a.example.com", "c.example.com", 10)));
			mgr.addRecord(workspaceId, ds.getId(), new Record(Arrays.asList((Object)"1.1.1.2", "10.1.1.1", "b.example.com", "c.example.com", 100)));
	      
	      client.close();
	}
	

	public Workspace getWorkspace(String id) {
		checkCentralIndexExists();
		GetResponse response = client.prepareGet(CENTRAL_INDEX, WORKSPACE_TYPE, id).execute().actionGet();

		Workspace ws = deserializeWorkspace(response.getSourceAsString());
		
		ws.setDatasets(getDatasetsForWorkspace(id));
		
		return ws;
	}
	
	private List<DataSet> getDatasetsForWorkspace(String id) {
		
		SearchResponse search = client.prepareSearch(id)
				.setTypes(DATASET_TYPE)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)

				 .setQuery(QueryBuilders.matchAllQuery()) //
				// Query
				//.setPostFilter(FilterBuilders.termFilter("dataset", datasetId)) // Filter
				.execute().actionGet();
		
		List<DataSet> ret = new ArrayList<DataSet>();
		
		for (SearchHit hit : search.getHits()) {
			DataSet ds = deserialize(hit.getSourceAsString(), DataSet.class);
			ds.setId(hit.getId());
			ret.add(ds);
		}
				
		return ret;
	}

	private String convert(Object object) {
		

		// generate json
		
		try {
			return mapper.writeValueAsString(object);
			
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public DataSet addDataset(String workspaceId, DataSet dataset) {
		String json = convert(dataset);
		if (json == null) {
			return null;
		}
		
		IndexResponse response = client.prepareIndex(workspaceId, DATASET_TYPE)
		        .setSource(json)
		        .execute()
		        .actionGet();
		
		dataset.setId(response.getId());
		return dataset;
	}
	
	public void addRecord(String workspaceId, String datasetId, Record record) {
		record.setDataset(datasetId);
		
		String json = convert(record);
		if (json == null) {
			return;
		}
		
		IndexResponse response = client.prepareIndex(workspaceId, RECORD_TYPE)
		        .setSource(json)
		        .execute()
		        .actionGet();
	}
	
	//TODO: may want to change this to only list ones I have access to
	public List<Workspace> getWorkspaces() {
		
		checkCentralIndexExists();
		
			List<Workspace> workspaces = new ArrayList<Workspace>();
		  SearchRequestBuilder search = client.prepareSearch(CENTRAL_INDEX)
			        .setTypes(WORKSPACE_TYPE)
			        .setQuery(QueryBuilders.matchAllQuery());

		SearchResponse response = search.execute().actionGet();
		
		System.out.println(response.getHits().getTotalHits());
		
		for(SearchHit hit : response.getHits()) {
			workspaces.add(deserializeWorkspace(hit.sourceAsString()));
			
		}
		
		return workspaces;
	}
	
	private <T> T deserialize(String json, Class<T> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			T obj = mapper.readValue(json, clazz);
		
			return obj;
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	private Workspace deserializeWorkspace(String workspaceJson) {
		return deserialize(workspaceJson, Workspace.class);
	}
	
	public String doQuery(String workspaceId, Query query) {
		DataSet dataset = new DataSet();
		dataset.setTimeStarted(new Date());
		
		dataset.setOriginatingQuery(query);
		
		dataset = addDataset(workspaceId, dataset);
		
		// Start off worker to do query
		// TODO: submit to pool
		Thread t = new Thread(new QueryRunnable(this, notificationManager, query, workspaceId, dataset.getId()));
		t.run();
		
		return dataset.getId();
	}
	
	public Workspace createWorkspaceIndex(String name) {
		Workspace ws = new Workspace(name);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		Date now = new Date();
		
		String indexName = UUID.randomUUID().toString(); //sdf.format(now);//
		
		ws.setCreated(new Date());
		ws.setId(indexName);
		
		// Create the index
		createIndex(indexName);

		//if (!createIndexResponse.isAcknowledged()) throw new Exception("Could not create index ["+indexName+"].");
		
		// Create the mappings
		// None yet
		
		// Add the entry to the workspace index
		// instance a json mapper
		createMapping(indexName, RECORD_TYPE, "/indices/workspace-mapping.json");
		
		String json = convert(ws);
		if (json == null) {
			return null;
		}
		
		checkCentralIndexExists();
		IndexResponse response = client.prepareIndex(CENTRAL_INDEX, WORKSPACE_TYPE, indexName)
		        .setSource(json)
		        .execute()
		        .actionGet();
		
		System.out.println("Creating index:");
		System.out.println(response.isCreated());
		
		return ws;
	}
	
	private void createIndex(String indexName) {
		CreateIndexRequestBuilder cirb = clientAdmin.indices().prepareCreate(indexName);
		CreateIndexResponse createIndexResponse = cirb.execute().actionGet();
	}

	private void checkCentralIndexExists() {
		if (!client.admin().indices().prepareExists(CENTRAL_INDEX).execute().actionGet().isExists()) {
			// Need to create
			System.err.println("Creating central index");
			
			createIndex(CENTRAL_INDEX);
			
			// Optionally now add mappings
		}
	}

	private void createMapping(String indexName, String type, String mappingSourceFile) {
		PutMappingRequestBuilder pmrb = clientAdmin.indices()
				.preparePutMapping(indexName)
				.setType(type);

		InputStream is = this.getClass().getResourceAsStream(mappingSourceFile);
		try {
			String mappingSource = IOUtils.toString(is);
			
			//System.err.println(mappingSource);
			
			pmrb.setSource(mappingSource);
			
			// Create type and mapping
			PutMappingResponse response = pmrb.execute().actionGet();	

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public DataSet getDataSet(String workspaceId, String datasetId) {
		
		GetResponse response = client.prepareGet(workspaceId, DATASET_TYPE, datasetId).execute().actionGet();

		DataSet ds = deserialize(response.getSourceAsString(), DataSet.class);
		ds.setId(response.getId());
		return ds;
	}
	
	public DataSet getFullyPopulatedDataSet(String workspaceId, String datasetId, int start, int length, String searchText, List<OrderSpec> ordering) {
		List<Record> recs = new ArrayList<Record>();
		DataSet ds = getDataSet(workspaceId, datasetId);
		String lastColumn = "";

		// Find the necessary ordering information

		CountResponse countResponse = client.prepareCount(workspaceId)
				.setTypes(WorkspaceManager.RECORD_TYPE)
				.setQuery(QueryBuilders.matchAllQuery())
				// TODO: replace this!
				//.setQuery(QueryBuilders.termQuery("dataset", datasetId))
				.execute().actionGet();

		SearchRequestBuilder search = client.prepareSearch(workspaceId)
				.setTypes(WorkspaceManager.RECORD_TYPE)
				.setSearchType(SearchType.DFS_QUERY_THEN_FETCH)
//TODO: replaace this!
				//.setPostFilter(FilterBuilders.termFilter("dataset", datasetId)) // Filter
				.setPostFilter(FilterBuilders.matchAllFilter())
				.setFrom(start).setSize(length);

		if (searchText != null) {
			// User is searching
			search.setQuery(QueryBuilders.prefixQuery("data",
					searchText));
		}

		// TODO: THis needs to understand the mappings to go through
		// As the columns don't map correctly otherwise
	
		for (OrderSpec order : ordering) {
			String sort = "_source.data[" + order.getColumn() + "]";

			ScriptSortBuilder sb = SortBuilders.scriptSort(sort,
					"string");

			if ("desc".equals(order.getDirection())) {
				sb.order(SortOrder.DESC);
			} else {
				sb.order(SortOrder.ASC);
			}
			search.addSort(sb);
		}

		SearchResponse response = search.execute().actionGet();

		ds.setRecordsFiltered(response.getHits().getTotalHits());
		ds.setRecordsTotal(countResponse.getCount());

		for (SearchHit hit : response.getHits()) {
			recs.add(new Record((List<Object>) hit.getSource().get("data")));
		}

		ds.setRecords(recs);
		
		return ds;
	}

}
