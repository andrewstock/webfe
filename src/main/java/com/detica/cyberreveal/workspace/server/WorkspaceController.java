package com.detica.cyberreveal.workspace.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.elasticsearch.action.count.CountResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.index.query.FilterBuilders;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.node.Node;
import org.elasticsearch.node.NodeBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.ScriptSortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.detica.cyberreveal.workspace.api.Attribute;
import com.detica.cyberreveal.workspace.api.DataSet;
import com.detica.cyberreveal.workspace.api.EntitySpec;
import com.detica.cyberreveal.workspace.api.OrderSpec;
import com.detica.cyberreveal.workspace.api.Projection;
import com.detica.cyberreveal.workspace.api.Record;
import com.detica.cyberreveal.workspace.api.RelationshipSpec;
import com.detica.cyberreveal.workspace.api.Workspace;

/**
 * Created with IntelliJ IDEA. User: cpa199 Date: 26/10/13 Time: 18:04 To change
 * this template use File | Settings | File Templates.
 */
@Controller
public class WorkspaceController extends AbstractDocumentController {

	@Autowired
	private Client client;
	
	@Autowired
	private WorkspaceManager manager;
	
	//@Autowired
	//private NotificationManager notificationManager = new NotificationManager();

	public WorkspaceController() {

//		if (client == null) {
//			ImmutableSettings.Builder settings = ImmutableSettings
//					.settingsBuilder();
//
//			settings.put("node.name", "orange11-node");
//
//			settings.put("path.data", "C:/devtools/index");
//
//			settings.put("http.enabled", true);
//
//			Node node = NodeBuilder.nodeBuilder()
//
//			.settings(settings)
//
//			.clusterName("orange11-cluster")
//
//			.data(false).local(false).node();
//
//			client = node.client();
//
//		}
//		manager = new WorkspaceManager(client, notificationManager);
	}

	@RequestMapping(value = "/workspaces", method = RequestMethod.GET)
	public @ResponseBody List<Workspace> getWorkspaces() {
		return manager.getWorkspaces();
	}

	@RequestMapping(value = "/workspaces/{id}", method = RequestMethod.GET)
	public @ResponseBody Workspace getWorkspace(@PathVariable String id) {
		return manager.getWorkspace(id);
	}

	@RequestMapping(value = "/workspaces/{workspaceId}/datasets/{datasetId}", method = RequestMethod.GET)
	public @ResponseBody DataSet getDataSet(@PathVariable String workspaceId,
			@PathVariable String datasetId,
			@RequestParam(value = "draw", defaultValue = "0") int draw,
			@RequestParam(value = "start", defaultValue = "0") int start,
			@RequestParam(value = "length", defaultValue = "10") int length,
			@RequestParam MultiValueMap<String, String> otherParams) {

		String searchText = null;
		List<String> searchValues = otherParams.get("search[value]");
		if (searchValues != null && searchValues.size() > 0) {
			// User is searching
			searchText = searchValues.get(0);
		}
		
		List<OrderSpec> ordering = new ArrayList<OrderSpec>();
		String lastColumn = "";
		
		for (String key : otherParams.keySet()) {
			if (key.startsWith("order[")) {
				// This is ordering information
				if (key.contains("column")) {
					lastColumn = otherParams.getFirst(key);
				} else {
					OrderSpec os = new OrderSpec(lastColumn, otherParams.getFirst(key));
					ordering.add(os);
				}
			}
		}
		
		DataSet ds = manager.getFullyPopulatedDataSet(workspaceId, datasetId, start, length, searchText, ordering);
		ds.setDraw(draw);

	
		//

		// "entities": [
		// {
		// "type": "ipaddress",
		// "role": "source",
		// "value": "<srcip>",
		// "attributes": [
		// {"name": "hostname", "value": "<srchost>"}
		// ]
		// },
		// {
		// "type": "ipaddress",
		// "role": "dest",
		// "value": "<destip>",
		// "attributes": [
		// {"name": "hostname", "value": "<desthost>"}
		// ]
		// }
		// ],
		// "relationships": [
		// {
		// "source": "ipaddress[source]",
		// "dest": "ipaddress[dest]",
		// "type": "comms.http",
		// "role": "http",
		// "attributes": [
		// {"name": "size", "value": "<size>"}
		// ]
		// }
		// ]
		// }
		// ],

		return ds;
	}

	// TODO: Need to allow more fields to be passed up
	// TODO: ensure we override certain values to ensure integrity
	@RequestMapping(value = "/workspaces", method = RequestMethod.POST)
	public @ResponseBody Workspace createWorkspaces(@RequestBody Workspace ws) {
		return manager.createWorkspaceIndex(ws.getName());
	}

	// @Autowired
	// private IDossier dossierImpl;
	//
	// @RequestMapping(value = "/{index}/dossier", method = RequestMethod.POST)
	// @ResponseBody
	// DocumentMetadata createDossier(@PathVariable String index, @RequestBody
	// Dossier dossier) throws IOException {
	// return this.dossierImpl.createDossier(index, dossier);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}", method =
	// RequestMethod.DELETE)
	// @ResponseBody
	// boolean deleteDossier(@PathVariable String index, @PathVariable String
	// id) throws IOException {
	// return this.dossierImpl.deleteDossier(index, id);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier", method = RequestMethod.GET)
	// @ResponseBody
	// SearchResults<Dossier> getDossier(@PathVariable String index,
	// @RequestParam(value = "from", defaultValue = "0") int from,
	// @RequestParam(value = "size", defaultValue = "10") int size,
	// @RequestParam(value = "sortField", required = false) String sortField,
	// @RequestParam(value = "sortOrder", defaultValue = "DESC") String
	// sortOrder,
	// @RequestParam(value = "mine", defaultValue = "false") boolean mine)
	// throws NotAllShardsRespondedException {
	// return this.dossierImpl.getDossier(index, from, size, sortField,
	// sortOrder, mine);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/childDocuments", method =
	// RequestMethod.POST)
	// @ResponseBody
	// long addDocument(@PathVariable String index, @PathVariable String id,
	// @RequestParam(value = "document") String[] documents) {
	// return this.dossierImpl.addDocument(index, id, documents);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/childDocuments", method =
	// RequestMethod.DELETE)
	// @ResponseBody
	// long removeDocument(@PathVariable String index, @PathVariable String id,
	// @RequestParam(value = "document") String[] documents) {
	// return this.dossierImpl.removeDocument(index, id, documents);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/childDocuments", method =
	// RequestMethod.GET)
	// @ResponseBody
	// SearchResults<Document> getChildren(@PathVariable String index,
	// @PathVariable String id)
	// throws DossierDoesNotExistException {
	// return this.dossierImpl.getAllChildren(index, id);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/attach", method =
	// RequestMethod.POST)
	// @ResponseBody
	// DocumentMetadata uploadBinary(@PathVariable String index, @PathVariable
	// String id, @RequestBody Attachment base64binary) {
	// return this.dossierImpl.uploadBinary(index, id, base64binary);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/attach", method =
	// RequestMethod.GET)
	// @ResponseBody
	// List<AttachmentMetadata> getBinaryNames(@PathVariable String index,
	// @PathVariable String id) throws NotAllShardsRespondedException {
	// return this.dossierImpl.getBinaryNames(index, id);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/attach/{attachid}", method
	// = RequestMethod.GET)
	// @ResponseBody
	// Attachment getBinary(@PathVariable String index, @PathVariable String id,
	// @PathVariable String attachid)
	// throws DossierDoesNotExistException {
	// return this.dossierImpl.getBinary(index, id, attachid);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/attach/{attachid}", method
	// = RequestMethod.DELETE)
	// @ResponseBody
	// boolean removeBinary(@PathVariable String index, @PathVariable String id,
	// @PathVariable String attachid)
	// throws DossierDoesNotExistException {
	// return this.dossierImpl.removeBinary(index, id, attachid);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/note", method =
	// RequestMethod.POST)
	// @ResponseBody
	// DocumentMetadata addNote(@PathVariable String index, @PathVariable String
	// id, @RequestBody String noteText) {
	// return this.dossierImpl.addNote(index, id, noteText);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/note/{noteid}", method =
	// RequestMethod.DELETE)
	// @ResponseBody
	// boolean removeNote(@PathVariable String index, @PathVariable String id,
	// @PathVariable String noteid) {
	// return this.dossierImpl.removeNote(index, id, noteid);
	// }
	//
	// @RequestMapping(value = "/{index}/dossier/{id}/note", method =
	// RequestMethod.GET)
	// @ResponseBody
	// SearchResults<DossierNote> getNotes(@PathVariable String index,
	// @PathVariable String id,
	// @RequestParam(value = "from", defaultValue = "0") int from,
	// @RequestParam(value = "size", defaultValue = "10") int size) throws
	// NotAllShardsRespondedException {
	// return this.dossierImpl.getNotes(index, id, from, size);
	// }

}
