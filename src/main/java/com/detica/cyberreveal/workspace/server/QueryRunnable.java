package com.detica.cyberreveal.workspace.server;

import com.detica.cyberreveal.workspace.api.Query;
import com.detica.cyberreveal.workspace.api.Record;

public class QueryRunnable implements Runnable {

	private Query query;
	private String workspaceId;
	private String datasetId;
	private WorkspaceManager manager;
	private NotificationManager notificationManager;

	public QueryRunnable(WorkspaceManager manager, NotificationManager notificationManager, Query query, String workspaceId, String datasetId) {
		this.query = query;
		this.workspaceId = workspaceId;
		this.datasetId = datasetId;
		this.manager = manager;
		this.notificationManager = notificationManager;
	}
	
	@Override
	public void run() {

		// Do the query
		
		// For result in results:
		Record record = new Record();
		manager.addRecord(workspaceId, datasetId, record);
		
		// Now notify that we're done
		notificationManager.sendMessage("query.complete", datasetId);
	}

}
