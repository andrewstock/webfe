package com.detica.cyberreveal.workspace.server;

import java.util.ArrayList;
import java.util.List;

public class NotificationManager {

	private List<NotificationListener> listeners = new ArrayList<NotificationListener>();
	
	public void sendMessage(String messageType, Object object) {
		notifyListeners(messageType, object);
	}
	
	private void notifyListeners(String messageType, Object object) {
		for (NotificationListener listener : listeners) {
			listener.messagedReceived(messageType, object);
		}
	}
	
	public void addListener(NotificationListener listener) {
		listeners.add(listener);
	}

	public void removeListener(NotificationListener listener) {
		listeners.remove(listener);
	}

}
