package com.detica.cyberreveal.elasticsearch;

import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;

public class ClientFactory implements FactoryBean<Client>,	InitializingBean, DisposableBean {

	private Client client;
	protected static Log logger = LogFactory.getLog(ClientFactory.class);
	protected Properties properties;
	protected String clusterName;
	
	private String[] esNodes =  { "localhost:9300" };
	
	/**
	 * Define ES nodes to communicate with.
	 * @return An array of nodes hostname:port
	 */
	public String[] getEsNodes() {
		return esNodes;
	}
	
	@Override
	public void destroy() throws Exception {
		try {
			logger.info("Closing ElasticSearch client");
			if (client != null) {
				client.close();
			}
		} catch (final Exception e) {
			logger.error("Error closing ElasticSearch client: ", e);
		}
	}

	public void setProperties(Properties properties) {
        this.properties = properties;
	}
	
	public void setClusterName(String clusterName) {
		this.clusterName = clusterName;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("Starting ElasticSearch client");
		
		client = buildClient();
	}
	
	protected Client buildClient() throws Exception {
        ImmutableSettings.Builder builder = ImmutableSettings.settingsBuilder();

        if (null != this.properties) {
            builder.put(this.properties);
        }
        
        builder.put("cluster.name", clusterName);

		TransportClient client = new TransportClient(builder.build());

		for (int i = 0; i < esNodes.length; i++) {
			client.addTransportAddress(toAddress(esNodes[i]));
		}

		return client;
	}
	
	/**
	 * Helper to define an hostname and port with a String like hostname:port
	 * @param address Node address hostname:port (or hostname)
	 * @return
	 */
	private InetSocketTransportAddress toAddress(String address) {
		if (address == null) return null;
		
		String[] splitted = address.split(":");
		int port = 9300;
		if (splitted.length > 1) {
			port = Integer.parseInt(splitted[1]);
		}
		
		return new InetSocketTransportAddress(splitted[0], port);
	}

	@Override
	public Client getObject() throws Exception {
		return client;
	}

	@Override
	public Class<?> getObjectType() {
		return Client.class;
	}

	@Override
	public boolean isSingleton() {
		return true;
	}

}
