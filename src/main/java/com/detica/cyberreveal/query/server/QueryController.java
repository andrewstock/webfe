package com.detica.cyberreveal.query.server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.detica.cyberreveal.query.api.QueryResult;
import com.detica.cyberreveal.workspace.api.Workspace;

@Controller
public class QueryController {

	  @RequestMapping(value = "/query", method = RequestMethod.GET)
	  public @ResponseBody
	  QueryResult getWorkspaces() {
		  QueryResult ret = new QueryResult();
		  
		  ret.setHeadings(Arrays.asList("srcip", "destip", "srchost", "desthost", "size"));
		  
		  List<List<Object>> data = new ArrayList<List<Object>>();
		  
		  data.add(Arrays.asList((Object)"1.1.1.1", "10.1.1.1", "a.example.com", "c.example.com", 10));
		  data.add(Arrays.asList((Object)"1.1.1.2", "10.1.1.1", "b.example.com", "c.example.com", 100));
		  
		  ret.setRecords(data);
		  
	      return ret;
	  }
}
