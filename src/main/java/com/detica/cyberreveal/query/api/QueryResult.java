package com.detica.cyberreveal.query.api;

import java.util.List;

public class QueryResult {
	private List<String> headings;
	private List<List<Object>> records;
	public List<String> getHeadings() {
		return headings;
	}
	public void setHeadings(List<String> headings) {
		this.headings = headings;
	}
	public List<List<Object>> getRecords() {
		return records;
	}
	public void setRecords(List<List<Object>> records) {
		this.records = records;
	}
	
	
}
