/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
/*
Provides support for standard ECMAScript features not present in the browser.
The goal is to support at least
- IE9
- Firefox 15
- Chrome 18
- Safari 5.1
- WebKit
- Opera 12
as defined by http://kangax.github.com/es5-compat-table/
 */

/*
 Declare Function.prototype.bind for browsers that don't support it.
 This is needed for Safari 5.1 versions prior to 5.1.7.
 See also https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function/bind
 */
(function(window){
if(!Function.prototype.bind) {
    Object.defineProperty(Function.prototype, 'bind', {
        value: function(that) {
            // implemented according to Mozilla Developer Network
            if(typeof this !== 'function') {
                throw new TypeError('Function.prototype.bind was not called on a function but: '+(typeof this));
            }

            var slice = Array.prototype.slice;
            var args = slice.call(arguments, 1);
            var noOperation = function() {};
            var fn = this;
            var bound = function() {
                return fn.apply(this instanceof noOperation ? this : that || window,
                    args.concat(slice.call(arguments)));
            };
            noOperation.prototype = this.prototype;
            bound.prototype = new noOperation();

            return bound;
        },
        writable: true, enumerable: false, configurable: true
    });
}

/*
 Define console and console.log() if they are not defined.
 This is needed for IE 9 and 10 if the developer tools are not open.
 */
if(!window.console) { window.console = {}; }
if(!window.console.log) { window.console.log = function(s) {}; }
}(self));
