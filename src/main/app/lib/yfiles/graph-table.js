/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies,f){f();}))(['./lang','./graph-table.mapping','./graph-table.impl','./core-lib.impl','./core-lib.mapping','./canvas-core.impl','./canvas-core.mapping','./graph-base.impl','./graph-base.mapping'],function(){});
