/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_pzc':["minimalNodeDistance","$Yj"],
      '_$_dif':["fixedRadius","$RJ"],
      '_$_yif':["layoutStyle","$jK"],
      '_$_slf':["initialAngle","$xL"],
      '_$_ipf':["minimalRadius","$pN"],
      '_$_tpf':["nodeSequencer","$AN"],
      '_$_osf':["fromSketchMode","$PO"],
      '_$_evf':["automaticRadius","$dQ"],
      '_$_ivf':["balloonLayouter","$hQ"],
      '_$_odg':["lastAppliedRadius","$XT"],
      '_$_egg':["considerNodeLabels","$aV"],
      '_$_tlg':["singleCycleLayouter","$CX"],
      '_$_vog':["partitionLayoutStyle","$aZ"],
      '_$_nqg':["fromSketchModeEnabled","$RZ"],
      '_$_xqg':["maximalDeviationAngle","$aab"],
      '_$_hah':["placeChildrenOnCommonRadius","$meb"],
      '_$_ppm':["CircularLayouter","GUC"],
      '_$_qpm':["LayoutStyle","HUC"],
      '_$_rpm':["PartitionLayoutStyle","IUC"],
      '_$_spm':["SingleCycleLayouter","JUC"],
      '_$$_gtb':["yfiles.circular","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
