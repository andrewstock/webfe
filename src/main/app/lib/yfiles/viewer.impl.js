/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies,f){f();}))(['./lang','./viewer.mapping','./core-lib','./canvas-core','./graph-base','./graph-binding','./graph-folding','./graph-graphml','./graph-input-table','./graph-input','./graph-style-control','./graph-style-defaults','./graph-style-extra','./graph-style-simple','./graph-style-table','./graph-table'],function(){{var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";if(myMappingsVersion){if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);}}}yfiles.module("yfiles._R", function (x) {});(function(Root,yfiles,window,undefined) {yfiles.module("yfiles._R", function(x){x.LFB=new yfiles.ClassDefinition(function(){return {};})});})(yfiles._R,yfiles,self);});