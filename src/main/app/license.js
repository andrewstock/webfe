(function(g){(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['yfiles/lang'],function(){
  g.yfiles = g.yfiles || {};
  yfiles.license = {
  "date": "07/01/2014",
  "distribution": false,
  "expires": "07/31/2014",
  "fileSystemAllowed": true,
  "licensefileversion": "1.1",
  "localhost": true,
  "oobAllowed": true,
  "package": "complete",
  "product": "yFiles for HTML",
  "type": "eval",
  "version": "1.2",
  "watermark": "yFiles HTML Evaluation License (expires in ${license-days-remaining} days)",
  "key": "63536c26134cef35a685801581f6286fdf7804b3"
}})}(self));