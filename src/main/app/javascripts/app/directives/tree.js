angular.module("app").directive('invTree', function() {
	
	var sel_data = [];
	
	return {
		restrict: 'A',
		scope: {
			treeData: '=',
			dblClick: '&'
		},
		template: '',
		link: function(scope, ele, attrs) {
			scope.$watch("treeData", function(value) {
				console.log("Change to treedata");
				console.log(scope.treeData);
				ele.jstree("destroy");
				ele.jstree({ 'core' : {
				    'data' : scope.treeData
				       },
				       "types" : {
				    	      "default" : {
				    	        "icon" : "glyphicon glyphicon-folder-open"
				    	      },
				    	      "graph" : {
				    	        "icon" : "glyphicon glyphicon-record"
				    	      },
				    	      "dataset" : {
				    	        "icon" : "glyphicon glyphicon-th-list"
				    	      },
				    	      "file" : {
				    	        "icon" : "glyphicon glyphicon-file"
				    	      }
				    	    },
				    	    "plugins" : [ "types" ]       
				});
				
				ele.on("changed.jstree", function (e, data) {
					 // console.log(data.selected);
				sel_data = data.selected;
				});
			
			ele.delegate("a","dblclick", function(e) {
//				console.log(sel_data);
//				var node = $(event.target).closest("li");
//				   var data = node.data("jstree");
//				   console.log("Data");
//				   console.log(data);
//				
//				  var idn = $(this).parent().attr("id").split("_")[1];

				  scope.dblClick({'item': sel_data[0]});
				});
				
            })
			
			
			return {
				pre: function(tElement, tAttrs, transclude) {
					//console.log(tElement);
				},
				post: function(scope, iElement, iAttrs, controller) {
					 // executed after the child elements are linked
					 // IS safe to do DOM transformations here
					 // same as the link function, if the compile option here we're
					 // omitted
				 }
			}
		}
		
	}
});