angular.module("app").directive('invEntity', function() {
	
	var render_entity = function ( data, type, full, meta ) {

		if (data.type) {
			return data.type + ': ' + data.value;
		}
	
      return data;
    };
	
	return {
		restrict: 'E',
		scope: {
			type: '@',
			value: '@',
			menu: '@'
		},
		template: '<b>{{ type }}</b>: {{ value }}',
		link: function(scope, ele, attrs) {
			//console.log(ele);
			//ele.find('table');
			//console.log("menu");
			//.log(scope.menu);
			
			ele.popover({
				trigger:"hover",
				title: "Title",
				content: 'Content here',
				placement: "auto right",
				delay: { show: 500, hide: 100 }
			});
			
			ele.contextmenu({
				  target:scope.menu, 
				  before: function(e,context) {
				    // execute code before context menu if shown
					 // console.log("BEFORE");
					  return true;
				  },
				  onItem: function(context,e) {
				    // execute on menu item selection

					  var table = context.parents('table').DataTable();
					  
					  var data = table.cell(context.parent()).data();
				  }
				});
			
			return {
				pre: function(tElement, tAttrs, transclude) {
					//console.log(tElement);
				},
				post: function(scope, iElement, iAttrs, controller) {
					 // executed after the child elements are linked
					 // IS safe to do DOM transformations here
					 // same as the link function, if the compile option here we're
					 // omitted
				 }
			}
		}
		
	}
});
