angular.module("app").directive('invTable', ['ProjectionService', '$compile', function(ProjectionService, $compile) {
	
	var render_entity = function ( data, type, full, meta ) {
		if (data.type) {
			return '<inv-entity type="' + data.type + '" value="' + data.value + '" menu="#context-menu"></inv-entity>';//data.type + ': ' + data.value;
		}
		return data;
    };
    
    var extract_entities = function(selection) {
    	var ents = [];
    	for (var i=0;i<selection.length;++i) {
    		for (var j=0; j<selection[i].length; ++j) {
    			
    			if (selection[i][j].type) { // current test for whether something is an entity
    				ents.push(selection[i][j]);
    			}
    		}
    	}
    	return ents;
    }
    
	var mapping =  [
		       	     {"name": "Source host", "value": "ipaddress[source].hostname|hostname"},
		    	     {"name": "Source IP", "value": "ipaddress[source]"},
		    	     {"name": "Destination host", "value": "ipaddress[dest].hostname|hostname"},
		    	     {"name": "Destination IP", "value": "ipaddress[dest]"},
		    	     {"name": "Size", "value": "[http].size"}
		    	];
	
	var columns = [];
	for (var i=0;i<mapping.length;++i) {
		columns.push({"title": mapping[i].name, "render": render_entity});
	}
	
	return {
		restrict: 'EA',
		template: '<table class="table table-striped table-bordered"></table>',
		scope: {
			selection: '&',
			workspace: '@',
			dataset: '&'
		},
		link: function(scope, ele, attrs) {
			var dataset = scope.dataset();
			console.log("Building table for " + dataset);
			console.log(dataset);
			console.log("ID is " + dataset.id);
			var table = ele.find('table').dataTable({

				"createdRow": function(row, data, dataIndex) {
					// Cause angular to compile the row after insertion - this allows
					// us to use directives in the row (as above where we create an inv-entity type
					 var linker = $compile(row);
					 var element = linker(scope);
					 row = element;
				},
				"columns": columns,
				"serverSide": true,
				"ajax": {
					"url":"../service/workspaces/" + scope.workspace + "/datasets/" + dataset.id,
					"dataSrc": function(data) {
						
						var proj = ProjectionService.projectData(data, data.projections[0]);
						
						// Allow customisation of data returned from server

						return ProjectionService.mapData(proj, mapping);
					}
				}
			});
			
			var tt = new $.fn.dataTable.TableTools( table, {
				"sRowSelect": "os",
				"aButtons": [ ]
	        } );
			 
		    $( tt.fnContainer() ).insertBefore('div.dataTables_wrapper');
			
		    table.on( 'click', 'tr', function () {

		    	scope.selection({"entities": extract_entities(tt.fnGetSelectedData())});

		    });
		    
			return {
				pre: function(tElement, tAttrs, transclude) {
				},
				post: function(scope, iElement, iAttrs, controller) {
					 // executed after the child elements are linked
					 // IS safe to do DOM transformations here
					 // same as the link function, if the compile option here we're
					 // omitted
				 }
			}
		}
	}
}]);
