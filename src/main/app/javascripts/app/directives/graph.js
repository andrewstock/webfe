angular.module("app").directive('invGraph', function() {

	var createGraph = function(node) {
		var /*yfiles.canvas.GraphControl*/ graphControl = new yfiles.canvas.GraphControl.ForDiv(node);  
		  
		// creates and configures a node style    
		var /**yfiles.drawing.ShinyPlateNodeStyle*/ nodeStyle = new yfiles.drawing.ShinyPlateNodeStyle.WithColor(yfiles.system.Colors.ORANGE);  
		nodeStyle.drawShadow = true;  
		// sets a default style    
		graphControl.graph.nodeDefaults.style = new yfiles.drawing.ShinyPlateNodeStyle.WithColor(yfiles.system.Colors.BLUE);  
		  
		var entities = {};
	
		var entityId = function(entity) {
			return entity.type + "--" + entity.role + "--" + entity.value;
		}
	
		var getEntity = function(entity_object, graph) {
	
			if (!(entityId(entity_object) in entities)) {
				console.log("Creating");
				console.log(entity_object);
				entities[entityId(entity_object)] = graph.createNodeWithBoundsAndStyle(new yfiles.geometry.RectD(10, 10, 100, 100), nodeStyle);  
				graph.addLabel(entities[entityId(entity_object)], entity_object.value);
				entities[entityId(entity_object)].tag = entity_object;
				
			} 
			return entities[entityId(entity_object)];
		}  
	
		// gets the IGraph    
		var /**yfiles.graph.IGraph*/ graph = graphControl.graph;
	/*
		var data = project_data(raw_data, projection2);
	
		for (var i=0;i<data.length;++i) {
			var datum = data[i];
			var mappings = {};
			
			Object.keys(datum.entities).forEach(function (key) {
				   // do something with obj[key]
	
				   var e = datum.entities[key];
				   mappings[e.type + "[" + e.role + "]"] = getEntity(datum.entities[key], graph);
				});
			
			
			
			//graph.createEdge(src, dest)
			
			for (var j=0; j<datum.relationships.length;++j) {
				var r = datum.relationships[j];
				var edge = graph.createEdge(mappings[r.source], mappings[r.dest]);
				graph.addLabel(edge, r.type);
				edge.tag = r;
				//getEntity(, graph);
			}
		}
	
	
		//non-blocking call  
		//doneHandler is executed when layout calculation is complete  
		graph.applyLayoutWithControlAndCallback(new yfiles.organic.SmartOrganicLayouter(), yfiles.system.TimeSpan.fromSeconds(2), graphControl, function() {});  
	*/
		// and creates some nodes    
		graph.createNodeWithBoundsAndStyle(new yfiles.geometry.RectD(10, 10, 100, 100), nodeStyle);  
		graph.createNodeWithBoundsAndStyle(new yfiles.geometry.RectD(150, 150, 100, 100), nodeStyle);  
		graph.createNodeWithBoundsAndStyle(new yfiles.geometry.RectD(250, 250, 100, 100), nodeStyle);  
		  
		graphControl.inputMode = new yfiles.input.GraphViewerInputMode();  
	
		/*graphControl.inputMode.selectionModel.addItemSelectedListener(function(src, evt) {
			console.log(evt.item.tag);
		});*/
	}    
    
	return {
		restrict: 'E',
//		scope: {
//			type: '@',
//			value: '@',
//			menu: '@'
//		},
		template: '<div style="width:100%;height: 100%"><div id="mygraph" style="position:relative;top:0px;left:0px;width:100%;height:100%"></div></div>',
		link: function(scope, ele, attrs) {
			createGraph(ele.find('div').get(1));
			
			return {
				pre: function(tElement, tAttrs, transclude) {
					//console.log(tElement);
				},
				post: function(scope, iElement, iAttrs, controller) {
					 // executed after the child elements are linked
					 // IS safe to do DOM transformations here
					 // same as the link function, if the compile option here we're
					 // omitted
				 }
			}
		}
		
	}
});
