angular.module("app").factory('ProjectionService', ['$http', '$q', function($http, $q) {
	
	var get_value = function(value, datum, col_indices) {
		if (value[0] == '<' && value[value.length-1]=='>') {
			return datum[col_indices[value.toLowerCase()]];
		} else {
			return value;
		}

	}

	var create_entity = function(datum, entity_spec, col_indices) {
		var entity = {};
		entity['type'] = get_value(entity_spec.type, datum, col_indices);
		entity['role'] = get_value(entity_spec.role, datum, col_indices);
		entity['value'] = get_value(entity_spec.value, datum, col_indices);
		entity.attributes = [];
		
		// Add attributes
		for (var i = 0; i < entity_spec.attributes.length; i++) {
			var attr = entity_spec.attributes[i];

		//	entity[attr.name] = get_value(attr.value, datum, col_indices);
			entity.attributes.push({"name": attr.name, "value": get_value(attr.value, datum, col_indices)});
		};

		return entity;
	}

	var create_relationship = function(datum, rel_spec, col_indices) {
		var rel = {};
		rel['source'] = get_value(rel_spec.source, datum, col_indices);
		rel['dest'] = get_value(rel_spec.dest, datum, col_indices);	
		rel['type'] = get_value(rel_spec.type, datum, col_indices);
		rel['role'] = get_value(rel_spec.role, datum, col_indices);
		rel.attributes = [];
		
		// Add attributes
		for (var i = 0; i < rel_spec.attributes.length; i++) {
			var attr = rel_spec.attributes[i];

			rel.attributes.push({"name": attr.name, "value": get_value(attr.value, datum, col_indices)});
		};
		return rel;
	}

	var project_datum = function(datum, projection, col_indices) {
		var entities = {};
		var relationships = [];

		for (var i = 0; i < projection.entities.length; i++) {
			var entity = create_entity(datum, projection.entities[i], col_indices);

			entities[entity.type + "[" + entity.role + "]"] = entity;
		};

		for (var i = 0; i < projection.relationships.length; i++) {
			var rel = create_relationship(datum, projection.relationships[i], col_indices);

			relationships.push(rel);
		};

		return {"entities": entities, "relationships": relationships};
	}

	var project_data = function(data, projection) {
		var projected_data = [];

		var col_indices = {};

//		console.log(data);
		
		// Map the headings for easier reference later
		for (var i = 0; i < data.headings.length; i++) {
			col_indices["<" + data.headings[i].toLowerCase() + ">"] = i;
		};

		// Project the data
		for (var i = 0; i < data.records.length; i++) {
			var proj = project_datum(data.records[i].data, projection, col_indices);
			projected_data.push(proj);
		};
		
		return projected_data;
	};
	
	var mapping = [
	      	     {"name": "Source host", "value": "ipaddress[source].hostname|hostname"},
	    	     {"name": "Source IP", "value": "ipaddress[source]"},
	    	     {"name": "Destination host", "value": "ipaddress[dest].hostname|hostname"},
	    	     {"name": "Destination IP", "value": "ipaddress[dest]"},
	    	     {"name": "Size", "value": "[http].size"}
	    	];
	
	var extract_mapping_spec = function(mapping_spec) {
		var patt = /^(\w+)?\[(\w+)\](\.(\w+)(\|(\w+))?)?$/;

		var match = patt.exec(mapping_spec);
//		console.log(mapping_spec);
	//	console.log(match);
		var result = {
			ent_type: match[1],
			role: match[2],
			attr: match[4],
			ent_cast: match[6]
		}

		return result;
	}
	
	var get_entity_by_type_role = function(entities, type, role) {
		
		return entities[type + "[" + role + "]"];
	
	};
	
	var get_rel_by_role = function(relationships, role) {
		for (var i=0; i<relationships.length; ++i) {
			if (relationships[i].role == role) {
				return relationships[i];
			}
		}
		
	//	console.log("Failed to find rel with role " + role);
	//	console.log(relationships);
		
		return null;
	}
	
	var map_datum_spec = function(datum, mapping_spec) {
		var item;
		if (mapping_spec.ent_type === undefined) {
			// Relationship
			item = get_rel_by_role(datum.relationships, mapping_spec.role);
		} else {
			item = get_entity_by_type_role(datum.entities, mapping_spec.ent_type, mapping_spec.role);
		}
		
		// Do we need an attribute?
		if (mapping_spec.attr !== undefined) {
	//		console.log("Looking for attr " + mapping_spec.attr);
	//		console.log(item);
			
			for (var i=0;i<item.attributes.length;++i) {
				if (item.attributes[i].name == mapping_spec.attr) {
					item = item.attributes[i].value;
					break;
				}
			}
			//item = item.attributes[mapping_spec.attr];
		}
		
		// Are we casting this to an entity?
		if (mapping_spec.ent_cast === undefined) {
			// no we're not
			return item;
		} else {
			return {'type': mapping_spec.ent_cast, 'value': item};
		}
	}
	
	var map_datum = function(datum, mapping_specs) {
		var ret = [];
		
		for (var i=0; i<mapping_specs.length;++i) {
			ret.push(map_datum_spec(datum, mapping_specs[i]));
		}
		
		return ret;
	};
	
	var map_data = function(data, mapping) {
		mapping_specs = [];
		
	//	console.log("mapping");
	//	console.log(mapping);
		
		for (var i=0; i<mapping.length;++i) {
			mapping_specs.push(extract_mapping_spec(mapping[i].value));
		}
		
		var ret = [];
		
		for (var i=0;i<data.length;++i) {
			ret.push(map_datum(data[i], mapping_specs));
		}
		
		return ret;
	};
	
	
	return {
		projectData: project_data,
		mapData: map_data
	};
}]);