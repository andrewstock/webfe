angular.module("app").factory('EnrichmentService', ['$http', '$q', function($http, $q) {
	
	var entities = [];
	
	var setSelection = function(ents) {
		entities = ents;
		
		console.log("Selection updated to");
		console.log(ents);
	}
	
	return {
		'setSelection': setSelection
	};
}]);