angular.module("app").factory('NotificationService', ['$http', '$q', function($http, $q) {
	
	var notifications = [];
	
	return {
		notifications: notifications,
		addNotification: function(not) {notifications.push(not);},
		activateNotification: function(index) {
			notifications[index].callback(notifications[index]);
			notifications.splice(index, 1); // remove notification
		}
	};
}]);