angular.module("app").controller('MainCtrl', ['$scope', 'NotificationService', function($scope, NotificationService) {
	$scope.notifications = NotificationService.notifications;
	
	$scope.activateNotification = function(index) {
		NotificationService.activateNotification(index);
	}
}]);