angular.module("app").controller('InvestigationCtrl', ['$scope', 'NotificationService', 'WorkspaceService', function($scope, NotificationService, WorkspaceService) {
	$scope.notifications = NotificationService.notifications;
	
	WorkspaceService.getWorkspaces().then(function(workspaces) {
		$scope.workspaces = workspaces;
	})
}]);