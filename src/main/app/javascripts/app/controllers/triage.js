angular.module("app").controller('TriageCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
	$scope.treedata = [
          'Simple root node',
          {
            'text' : 'Root node 4',
            'state' : {
              'opened' : true,
              'selected' : true
            },
            'children' : [
              { 'text' : 'Child 1' },
              'Child 2'
            ]
         }
       ];

}]);