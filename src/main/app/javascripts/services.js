raw_data = {
	"headings": ["srcip", "destip", "srchost", "desthost", "size"],
	"records": [
		['1.1.1.1', '10.1.1.1', 'a.example.com', 'c.example.com', 10],
		['1.1.1.2', '10.1.1.1', 'b.example.com', 'c.example.com', 100]
	]
};



var val = "ipaddress[source].hostname|hostname";

var patt = /^(\w+)?\[(\w+)\](\.(\w+)(\|(\w+))?)?$/;
//var patt = new RegExp("^(\w+)?\[(\w+)\](\.(\w+)(\|(\w+))?)?$");

var match = patt.exec(val);

var result = {
	ent_type: match[1],
	role: match[2],
	attr: match[4],
	ent_cast: match[6]
}

console.log(result);

console.log();