var app = angular.module("app", ['ui.router', 'restangular', 'ui.bootstrap']);

app.config(function(RestangularProvider) {
	RestangularProvider.setBaseUrl('/com.cyberreveal.workspace-server/service/');
});

angular.module("app").config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/start");

    $stateProvider.state('triage', {
        url: '/triage',
        templateUrl: 'partials/triage.html',
        controller: 'TriageCtrl'
        	
    }).state('ti', {
        url: '/ti',
        templateUrl: 'partials/ti.html',
        controller: 'TriageCtrl'
        	
    }).state('invest', {
        url: '/investigate',
        templateUrl: 'partials/investigate.html',
        controller: 'InvestigationCtrl'
        	
    }).state('invest.ws', {
    	url: '/:workspaceId',
    	templateUrl: 'partials/workspace.html',
    	controller: 'WorkspaceCtrl'
    }).state('start', {
        url: '/start',
        templateUrl: 'partials/start.html',
        controller: 'StartCtrl'
    });
});


angular.module("app").controller('StartCtrl', ['$scope', '$stateParams', function($scope, $stateParams) {
		
	   
}]);

