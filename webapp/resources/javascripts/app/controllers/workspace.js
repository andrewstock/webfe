angular.module("app").controller('WorkspaceCtrl', ['$scope', '$stateParams', 'Restangular', 'WorkspaceService', 'EnrichmentService', 'NotificationService', function($scope, $stateParams, Restangular, WorkspaceService, EnrichmentService, NotificationService) {
	console.log("workspace!");
	console.log($stateParams);
	//console.log(Restangular);
	
	$scope.workspaceId = $stateParams.workspaceId;
	
	$scope.tabs = [];
	
	var convert = function(list, typ) {
		var ret = [];
		for (var i=0; i<list.length; ++i) {
			ret.push({'text':list[i], 'type': typ, 'id': typ + "-" + i});
		}
		return ret;
	};
	
	var convert_ds = function(list, typ) {
		var ret = [];
		for (var i=0; i<list.length; ++i) {
			ret.push({'text':list[i].id, 'type': typ, 'id': typ + "-" + i});
		}
		return ret;
	};
	
	WorkspaceService.getDetails($stateParams.workspaceId).then(function(ws){
		console.log(ws);
		console.log(convert_ds(ws.datasets, 'dataset'));
		$scope.workspace = ws;
		$scope.treedata = [
		                   {'text': 'Files', 'children': convert(ws.files, 'file')},
		                   {'text': 'Datasets', 'children': convert_ds(ws.datasets, 'dataset')},
		                   {'text': 'Graphs', 'children': convert(ws.graphs, 'graph')},
		    ];
	});
	
	$scope.notifications = NotificationService.notifications;
	
	$scope.addNotification = function() {
		NotificationService.addNotification({'message': 'Hello'});
	}
	
	$scope.treedata = [
	                   
	                ];
	
	$scope.itemDblClick = function(item) {
		

		
		//alert("Clicked: " + item);
		$scope.$apply(function() {
			// check first to see if this tab is open
			var done = false;
			for (var i=0;i<$scope.tabs.length;++i) {
				if ($scope.tabs[i].ref == item) {
					$scope.tabs[i].active = true;
					done = true;
					break;
				}
			}
			if (!done) {
				console.log("Double click");
				console.log(item);
				
				var parts = item.split('-');
				var item_type = parts[0];
				var item_id = parts[1];
				
				var tab = {title: item, type:item_type, ref:item};
				
				if (item_type == 'dataset') {
					// Need to add which it is
					tab.dataset = $scope.workspace.datasets[item_id];
					console.log(tab.dataset);
				}
		//		if (item == '4') {
		//			tab.type='dataset';
		//		}
				
				NotificationService.addNotification({"message": "Table is ready", "callback": function() {
					tab.active = true;
				}});
					
				$scope.tabs.push(tab);
			}
		});

	}
	
	$scope.removeTab = function(index) {
		//alert(index);
		$scope.tabs.splice(index, 1);
	}

	$scope.tableSelection = function(entities) {
		EnrichmentService.setSelection(entities);
	}
	
}]);