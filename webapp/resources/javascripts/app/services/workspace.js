angular.module("app").factory('WorkspaceService', ['$http', '$q', 'Restangular', function($http, $q, Restangular) {
	var current_user;
	
	return {
		getWorkspaces: function() {
			var d = $q.defer();
			
			Restangular.all("workspaces").getList().then(function(workspaces) {
				d.resolve(workspaces);
			})
			
			return d.promise;
		},
		getDetails: function(workspaceId) {
			console.log("Getting details for workspace: " + workspaceId);
			var d = $q.defer();
		
			Restangular.one('workspaces', workspaceId).get().then(function(ws) {
				d.resolve(ws);
			});
			//	console.log(ws);
			//});
			
			
			//d.resolve({"name":"One","id":"","created":1404852514252,"files":["File 1","File 2","File 3"],"graphs":["Graph 1","Graph 2","Graph 3"],"datasets":["dataset0","DS 2","DS 3"]});
			
			return d.promise;
		},
		getCurrentUser: function() {
			return current_user;
		},
		setCurrentUser: function(user) {
			current_user = user;
		}
	 }
 }]);
