/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_uvb':["NodeInfo","$a"],
      '_$_ntc':["radius","$Wg"],
      '_$_xzc':["minimalLayerDistance","$bk"],
      '_$_ngf':["sectorSize","$bJ"],
      '_$_lhf':["circleIndex","$AJ"],
      '_$_wjf':["sectorStart","$EK"],
      '_$_qkf':["centerOffset","$XK"],
      '_$_wlf':["layerSpacing","$CL"],
      '_$_xyf':["centerNodesDpKey","$NR"],
      '_$_hag':["layeringStrategy","$wS"],
      '_$_lag':["minimalBendAngle","$AS"],
      '_$_dcg':["centerNodesPolicy","$rT"],
      '_$_agg':["considerNodeLabels","$aV"],
      '_$_bkg':["edgeRoutingStrategy","$NW"],
      '_$_xug':["maximalChildSectorAngle","$Obb"],
      '_$_gyg':["minimalNodeToNodeDistance","$ndb"],
      '_$_ock':["determineCenter","$HPb"],
      '_$_lrk':["getNodeDiameter","$vWb"],
      '_$_ybn':["CenterNodesPolicy","PGD"],
      '_$_zbn':["LayeringStrategy","QGD"],
      '_$_acn':["EdgeRoutingStrategy","RGD"],
      '_$_bcn':["RadialLayouter","SGD"],
      '_$$_bub':["yfiles.radial","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
