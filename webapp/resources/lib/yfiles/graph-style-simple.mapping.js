/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_lbd':["getPath","$Gk"],
      '_$_pcd':["getOutline","$Yk"],
      '_$_qdd':["getSegmentCount","$tl"],
      '_$_ted':["lookup","$Wl"],
      '_$_pgd':["isInside","$Jm"],
      '_$_xgd':["getBounds","$Om"],
      '_$_yid':["createVisual","$Dn"],
      '_$_yud':["isHit","$Ks"],
      '_$_qvd':["isInBox","$Zs"],
      '_$_nxd':["isVisible","$Ft"],
      '_$_jzd':["updateVisual","$nu"],
      '_$_eae':["getIntersection","$Du"],
      '_$_lae':["getPreferredSize","$Hu"],
      '_$_mde':["getTangent","$Zv"],
      '_$_uvj':["cropPath","$MMb"],
      '_$_ybl':["arrangeByLayout","$pbc"],
      '_$_hdl':["getSourceArrowAnchor","$Wbc"],
      '_$_jdl':["getTargetArrowAnchor","$Xbc"],
      '_$_ggl':["getTangentForSegment","$tdc"],
      '_$_xgl':["addArrows","$Kdc"],
      '_$_ywl':["SimpleAbstractEdgeStyle","KZB"],
      '_$_zwl':["SimpleAbstractLabelStyle","LZB"],
      '_$_axl':["SimpleAbstractNodeStyle","MZB"],
      '_$_bxl':["SimpleAbstractPortStyle","NZB"],
      '_$$_lqb':["yfiles.drawing","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
