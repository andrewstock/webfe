/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_gwb':["getBounds","$a"],
      '_$_hwb':["Matrix","$a"],
      '_$_iwb':["RootAlignment","$b"],
      '_$_jwb':["translatePoint","$c"],
      '_$_kwb':["WithOrientationAndAspectRatio","$a"],
      '_$_lwb':["WithMatrix","$a"],
      '_$_mwb':["createCompoundComparator","$b"],
      '_$_nwb':["WithDistance","$a"],
      '_$_owb':["WithAlignmentAndDistance","$b"],
      '_$_pwb':["WithAlignmentStyleAndDistance","$c"],
      '_$_qwb':["WithAlignmentStyleDistanceLengthAndSlope","$d"],
      '_$_rwb':["WithMode","$a"],
      '_$_swb':["WithModeAndRatio","$b"],
      '_$_twb':["SubtreeShape","$a"],
      '_$_uwb':["WithMatrixAndAlignment","$b"],
      '_$_vwb':["WithMatrixAndId","$a"],
      '_$_wwb':["LeftRightDataProvider","$a"],
      '_$_xwb':["WithMatrix","$b"],
      '_$_ywb':["WithMatrixAndRootAlignment","$b"],
      '_$_ooc':["id","$Qf"],
      '_$_usc':["bounds","$Mg"],
      '_$_ozc':["minimalNodeDistance","$Yj"],
      '_$_wzc':["minimalLayerDistance","$bk"],
      '_$_edd':["postProcess","$jl"],
      '_$_ged':["createFromSketchComparator","$Kl"],
      '_$_eyd':["assignPorts","$Tt"],
      '_$_ace':["determineChildConnectors","$vv"],
      '_$_ode':["preProcess","$bw"],
      '_$_cee':["createProcessor","$nw"],
      '_$_sge':["placeSubtree","$ux"],
      '_$_pse':["maxX","$SC"],
      '_$_tse':["maxY","$TC"],
      '_$_yse':["minX","$WC"],
      '_$_ate':["minY","$XC"],
      '_$_dte':["mode","$YC"],
      '_$_pxe':["originX","$ME"],
      '_$_rxe':["originY","$NE"],
      '_$_aye':["spacing","$VE"],
      '_$_daf':["minSlope","$VF"],
      '_$_jbf':["createBus","$CG"],
      '_$_xbf':["fillStyle","$OG"],
      '_$_mdf':["portStyle","$AH"],
      '_$_ief':["comparator","$WH"],
      '_$_lef':["connectorX","$XH"],
      '_$_mef':["connectorY","$YH"],
      '_$_nef':["coreBounds","$ZH"],
      '_$_zef':["horizontal","$mI"],
      '_$_hhf':["aspectRatio","$wJ"],
      '_$_wif':["layoutStyle","$jK"],
      '_$_ojf':["orientation","$xK"],
      '_$_okf':["bendDistance","$VK"],
      '_$_pkf':["busAlignment","$WK"],
      '_$_vlf':["layerSpacing","$CL"],
      '_$_qmf':["routingStyle","$UL"],
      '_$_gqf':["rootAlignment","$MN"],
      '_$_hqf':["rootPlacement","$NN"],
      '_$_kqf':["routingPolicy","$QN"],
      '_$_sqf':["verticalSpace","$YN"],
      '_$_hrf':["childPlacement","$nO"],
      '_$_ttf':["minSlopeHeight","$sP"],
      '_$_jvf':["childNodePlacer","$iQ"],
      '_$_svf':["dendrogramStyle","$rQ"],
      '_$_ewf':["horizontalSpace","$DQ"],
      '_$_jxf':["placerUpperLeft","$eR"],
      '_$_bbg':["placerLowerRight","$PS"],
      '_$_rbg':["verticalDistance","$gT"],
      '_$_tcg':["defaultLeafPlacer","$GT"],
      '_$_ucg':["defaultNodePlacer","$HT"],
      '_$_edg':["groupingSupported","$QT"],
      '_$_ceg':["nonTreeEdgeRouter","$jU"],
      '_$_jeg':["placeLastOnBottom","$qU"],
      '_$_seg':["reversedPortOrder","$xU"],
      '_$_jfg':["verticalAlignment","$NU"],
      '_$_ufg':["connectorDirection","$YU"],
      '_$_wgg':["horizontalDistance","$rV"],
      '_$_hhg':["modificationMatrix","$CV"],
      '_$_rkg':["minimumRootDistance","$bX"],
      '_$_umg':["childPlacementPolicy","$dY"],
      '_$_log':["minLastSegmentLength","$QY"],
      '_$_fqg':["defaultPortAssignment","$JZ"],
      '_$_hqg':["enforceGlobalLayering","$LZ"],
      '_$_zqg':["minFirstSegmentLength","$cab"],
      '_$_isg':["defaultChildComparator","$Kab"],
      '_$_ysg':["integratedEdgeLabeling","$Wab"],
      '_$_atg':["integratedNodeLabeling","$Xab"],
      '_$_gtg':["minimumSubtreeDistance","$bbb"],
      '_$_hug':["borderGapToPortGapRatio","$zbb"],
      '_$_zug':["nonTreeEdgeSelectionKey","$Qbb"],
      '_$_bvg':["polylineLabelingEnabled","$Sbb"],
      '_$_uyg':["treeComponentCoreLayouter","$Adb"],
      '_$_ibh':["orientationOptimizationActive","$Leb"],
      '_$_agh':["createCopy","$Pgb"],
      '_$_ajh':["createComparator","$pib"],
      '_$_hkh':["updateConnectorShape","$bjb"],
      '_$_qkh':["createStraightlineRouter","$mjb"],
      '_$_noh':["multiply","$glb"],
      '_$_uph':["mergeWith","$Nlb"],
      '_$_hth':["equalValues","$mnb"],
      '_$_nwh':["reverseEdges","$Lob"],
      '_$_oxh':["getBorderLine","$lpb"],
      '_$_lfi':["getSourcePointAbs","$Hsb"],
      '_$_pgi':["appendTargetPoints","$ltb"],
      '_$_rti':["move","$dzb"],
      '_$_bzi':["addTargetPoint","$LBb"],
      '_$_dzi':["assignValuesTo","$NBb"],
      '_$_tzi':["addEdgeSegments","$dCb"],
      '_$_igj':["addLineSegment","$xFb"],
      '_$_pgj':["addBoundsToShape","$EFb"],
      '_$_rjj':["sourcePortConstraintDataAcceptor","$fHb"],
      '_$_sjj':["targetPortConstraintDataAcceptor","$gHb"],
      '_$_skj':["directTree","$DHb"],
      '_$_vwj':["layoutRoot","$mNb"],
      '_$_cyj':["getNodeShape","$PNb"],
      '_$_yyj':["getNodePlacer","$iOb"],
      '_$_zyj':["getRootsArray","$jOb"],
      '_$_azj':["getSuccessors","$kOb"],
      '_$_sak':["getAspectRatio","$UOb"],
      '_$_wak':["isVerticalRoot","$YOb"],
      '_$_tck':["getSubtreeShape","$LPb"],
      '_$_iek':["getRootPlacement","$sQb"],
      '_$_jek':["getRoutingPolicy","$tQb"],
      '_$_lek':["isHorizontalRoot","$vQb"],
      '_$_rfk':["getPortAssignment","$aRb"],
      '_$_ufk':["getPortConstraint","$cRb"],
      '_$_bhk':["createSubtreeShape","$GRb"],
      '_$_pik':["createRootNodeShape","$pSb"],
      '_$_zlk':["determineChildConnector","$TTb"],
      '_$_ank':["translateDirectionToReal","$qUb"],
      '_$_onk':["translateDirectionToModel","$CUb"],
      '_$_ynk':["getChildNodeInEdgeComparator","$MUb"],
      '_$_zpk':["placeSubtreeWithDirection","$MVb"],
      '_$_aqk':["placeSubtreeImpl","$NVb"],
      '_$_fsk':["getPortBorderGap","$PWb"],
      '_$_ktk':["routeNonTreeEdges","$sXb"],
      '_$_luk':["doLayoutUsingDummies","$SXb"],
      '_$_suk':["getUpperLeftChildren","$ZXb"],
      '_$_cvk':["getLowerRightChildren","$jYb"],
      '_$_zvk':["getTargetPortConstraint","$EYb"],
      '_$_bdl':["calcSourceEdgeLayout","$Sbc"],
      '_$_cdl':["calcTargetEdgeLayout","$Tbc"],
      '_$_edl':["getPortDistanceDelta","$Vbc"],
      '_$_sdl':["getSourcePortConstraint","$gcc"],
      '_$_udl':["assignParentEdgeTargetPort","$icc"],
      '_$_lfl':["placeParentHorizontal","$Ycc"],
      '_$_vfl':["assignChildEdgeSourcePort","$idc"],
      '_$_hgl':["createBends","$udc"],
      '_$_tgl':["calcSlopedSourceEdgeLayout","$Gdc"],
      '_$_bhl':["calcParentConnector","$Odc"],
      '_$_scn':["AbstractNodePlacer","JHD"],
      '_$_tcn':["AbstractRotatableNodePlacer","KHD"],
      '_$_ucn':["ARNodePlacer","LHD"],
      '_$_vcn':["ARTreeLayouter","MHD"],
      '_$_wcn':["AssistantPlacer","NHD"],
      '_$_xcn':["BusPlacer","OHD"],
      '_$_ycn':["DefaultNodePlacer","PHD"],
      '_$_zcn':["DefaultPortAssignment","QHD"],
      '_$_adn':["DelegatingNodePlacer","RHD"],
      '_$_bdn':["DendrogramPlacer","SHD"],
      '_$_cdn':["DoubleLinePlacer","THD"],
      '_$_ddn':["FreePlacer","UHD"],
      '_$_edn':["GenericTreeLayouter","VHD"],
      '_$_fdn':["GridNodePlacer","WHD"],
      '_$_gdn':["HierarchicTreePlacer","XHD"],
      '_$_hdn':["HVTreeLayouter","YHD"],
      '_$_idn':["IFromSketchNodePlacer","ZHD"],
      '_$_jdn':["INodePlacer","AID"],
      '_$_kdn':["IPortAssignment","BID"],
      '_$_ldn':["IProcessor","CID"],
      '_$_mdn':["LayeredNodePlacer","DID"],
      '_$_ndn':["LeafPlacer","EID"],
      '_$_odn':["LeftRightPlacer","FID"],
      '_$_pdn':["NodeOrderComparator","GID"],
      '_$_qdn':["SimpleNodePlacer","HID"],
      '_$_rdn':["SubtreeShapeRotated","IID"],
      '_$_sdn':["TreeComponentLayouter","JID"],
      '_$_tdn':["TreeLayouter","KID"],
      '_$_udn':["TreeReductionStage","LID"],
      '_$_vdn':["XCoordComparator","MID"],
      '_$$_dub':["yfiles.tree","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
