/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies,f){f();}))(['./lang','./viewer.mapping','./viewer.impl','./core-lib.impl','./core-lib.mapping','./canvas-core.impl','./canvas-core.mapping','./graph-base.impl','./graph-base.mapping','./graph-binding.impl','./graph-binding.mapping','./graph-folding.impl','./graph-folding.mapping','./graph-graphml.impl','./graph-graphml.mapping','./graph-input-table.impl','./graph-input-table.mapping','./graph-input.impl','./graph-input.mapping','./graph-table.impl','./graph-table.mapping','./graph-style-control.impl','./graph-style-control.mapping','./graph-style-defaults.impl','./graph-style-defaults.mapping','./graph-style-extra.impl','./graph-style-extra.mapping','./graph-style-simple.impl','./graph-style-simple.mapping','./graph-style-table.impl','./graph-style-table.mapping'],function(){});
