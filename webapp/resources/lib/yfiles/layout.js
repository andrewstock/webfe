/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies,f){f();}))(['./lang','./layout.mapping','./layout.impl','./algorithms.impl','./algorithms.mapping','./core-lib.impl','./core-lib.mapping','./layout-util.impl','./layout-util.mapping','./layout-circular.impl','./layout-circular.mapping','./layout-organic.impl','./layout-organic.mapping','./layout-core.impl','./layout-core.mapping','./layout-hierarchic.impl','./layout-hierarchic.mapping','./layout-router.impl','./layout-router.mapping','./canvas-core.impl','./canvas-core.mapping','./layout-tree.impl','./layout-tree.mapping','./layout-misc.impl','./layout-misc.mapping','./layout-multipage.impl','./layout-multipage.mapping','./layout-partial.impl','./layout-partial.mapping','./layout-orthogonal.impl','./layout-orthogonal.mapping','./layout-planar.impl','./layout-planar.mapping','./layout-polyline.impl','./layout-polyline.mapping'],function(){});
