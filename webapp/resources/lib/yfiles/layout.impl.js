/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies,f){f();}))(['./lang','./layout.mapping','./algorithms','./layout-circular','./layout-core','./layout-hierarchic','./layout-misc','./layout-multipage','./layout-organic','./layout-orthogonal','./layout-partial','./layout-planar','./layout-router','./layout-tree','./layout-util'],function(){{var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";if(myMappingsVersion){if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);}}}yfiles.module("yfiles._R", function (x) {});(function(Root,yfiles,window,undefined) {yfiles.module("yfiles._R", function(x){x.KFB=new yfiles.ClassDefinition(function(){return {};})});})(yfiles._R,yfiles,self);});