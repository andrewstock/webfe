/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_upc':["path","$ig"],
      '_$_crc':["graph","$vg"],
      '_$_ywc':["edgeDefaults","$Di"],
      '_$_bxc':["nodeDefaults","$Fi"],
      '_$_yyc':["groupNodeDefaults","$Jj"],
      '_$_ecd':["createEdge","$Tk"],
      '_$_kcd':["createNode","$Uk"],
      '_$_nje':["createGroupNode","$Hy"],
      '_$_ibf':["converter","$BG"],
      '_$_icf':["idBinding","$YG"],
      '_$_aif':["edgesSource","$OJ"],
      '_$_mjf':["nodesSource","$wK"],
      '_$_rkf':["childBinding","$YK"],
      '_$_qlf':["groupBinding","$vL"],
      '_$_rlf':["groupsSource","$wL"],
      '_$_hof':["edgeIdBinding","$OM"],
      '_$_qpf':["nodeIdBinding","$xN"],
      '_$_tsf':["groupIdBinding","$UO"],
      '_$_ctf':["inEdgesBinding","$cP"],
      '_$_dxf':["outEdgesBinding","$YQ"],
      '_$_mzf':["edgeLabelBinding","$bS"],
      '_$_jag':["locationXBinding","$yS"],
      '_$_kag':["locationYBinding","$zS"],
      '_$_qag':["nodeLabelBinding","$FS"],
      '_$_xeg':["sourceNodeBinding","$CU"],
      '_$_yeg':["successorsBinding","$DU"],
      '_$_bfg':["targetNodeBinding","$GU"],
      '_$_ggg':["converterParameter","$cV"],
      '_$_ehg':["lazyNodeDefinition","$zV"],
      '_$_shg':["parentGroupBinding","$LV"],
      '_$_elg':["predecessorsBinding","$mX"],
      '_$_wfh':["buildGraph","$Ngb"],
      '_$_vgh':["updateGraph","$nhb"],
      '_$_coh':["evaluate","$Xkb"],
      '_$_dcl':["updateGroupNode","$ubc"],
      '_$_mel':["updateEdge","$zcc"],
      '_$_nel':["updateNode","$Acc"],
      '_$_crl':["AdjacentEdgesGraphSource","ASB"],
      '_$_drl':["AdjacentNodesGraphSource","BSB"],
      '_$_erl':["Binding","CSB"],
      '_$_frl':["GraphSource","DSB"],
      '_$_grl':["GraphSourceBase","ESB"],
      '_$_hrl':["TreeSource","FSB"],
      '_$$_lob':["yfiles.binding","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
