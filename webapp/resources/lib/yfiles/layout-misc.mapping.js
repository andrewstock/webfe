/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_dub':["WithSize","$a"],
      '_$_eub':["FromOuterAndInnerStage","$a"],
      '_$_fub':["FromOrientedRectangle","$a"],
      '_$_gub':["FromRectangle","$b"],
      '_$_hub':["WithCoreLayouter","$a"],
      '_$_iub':["findReversedTreeEdges","$b"],
      '_$_jub':["ForSize","$a"],
      '_$_kub':["fillComparableMapFromGraph","$b"],
      '_$_spc':["node","$hg"],
      '_$_xzd':["calculateBounds","$zu"],
      '_$_lve':["mirror","$QD"],
      '_$_yxe':["spacing","$VE"],
      '_$_ocf':["layouters","$gH"],
      '_$_gef':["columnMode","$UH"],
      '_$_uef':["fixedWidth","$gI"],
      '_$_zhf':["edgeSpacing","$NJ"],
      '_$_ckf':["targetRatio","$JK"],
      '_$_ylf':["layoutStages","$FL"],
      '_$_xqf':["adoptSelection","$cO"],
      '_$_ksf':["fixPointPolicy","$NO"],
      '_$_btf':["includingEdges","$bP"],
      '_$_avf':["adoptEdgeGroups","$ZP"],
      '_$_gwf':["includingLabels","$FQ"],
      '_$_mig':["selectedEdgesDpKey","$cW"],
      '_$_mmg':["adoptPortConstraints","$VX"],
      '_$_ong':["fixedWidthLineBreaks","$xY"],
      '_$_qjh':["clearLayouterChain","$Gib"],
      '_$_xsh':["appendStage","$dnb"],
      '_$_fuh':["reverseEdge","$Knb"],
      '_$_dwh':["prependStage","$Cob"],
      '_$_lwh':["reverseEdges","$Lob"],
      '_$_uyh':["appendLayouter","$Qpb"],
      '_$_uai':["appendLayouters","$Lqb"],
      '_$_kvi':["equalsEps","$Uzb"],
      '_$_rvj':["prepare","$LMb"],
      '_$_qwj':["unprepare","$hNb"],
      '_$_nak':["calculatePorts","$POb"],
      '_$_jfk':["calculateFixPoint","$RQb"],
      '_$_tsk':["calculateFixPointForNodes","$cXb"],
      '_$_ecl':["addedPathForEdge","$vbc"],
      '_$_pym':["BendConverter","GDD"],
      '_$_qym':["CompositeLayoutStage","HDD"],
      '_$_rym':["DefaultNodeLabelLayout","IDD"],
      '_$_sym':["EdgeOppositeNodeLabelLayoutModel","JDD"],
      '_$_tym':["EdgeReversalStage","KDD"],
      '_$_uym':["FixNodeLayoutStage","LDD"],
      '_$_vym':["GraphLayoutLineWrapper","MDD"],
      '_$_wym':["MinNodeSizeStage","NDD"],
      '_$_xym':["NormalizingGraphElementOrderStage","ODD"],
      '_$_yym':["PortCalculator","PDD"],
      '_$_zym':["PortConstraintEnforcementStage","QDD"],
      '_$_azm':["SequentialLayouter","RDD"],
      '_$$_ttb':["yfiles.layout","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
