/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_gk':["loadAllTemplates","$a"],
      '_$_hk':["WithSvgContent","$a"],
      '_$_ik':["WithKey","$a"],
      '_$_nsc':["width","$Lg"],
      '_$_btc':["height","$Pg"],
      '_$_xkd':["getMinimumSize","$so"],
      '_$_zvd':["setSize","$et"],
      '_$_lzd':["createTemplateContext","$pu"],
      '_$_jae':["getPreferredSize","$Hu"],
      '_$_uue':["center","$AD"],
      '_$_iwe':["content","$kE"],
      '_$_naf':["templateStyleRenderer","$fG"],
      '_$_vdg':["mapperRegistryTag","$cU"],
      '_$_oyh':["setSvgElement","$Kpb"],
      '_$_dui':["getTag","$nzb"],
      '_$_vlj':["getRenderSize","$hIb"],
      '_$_bmj':["createRenderer","$oIb"],
      '_$_hvj':["arrange","$CMb"],
      '_$_hck':["setArrangeRect","$APb"],
      '_$_rck':["getOutlineShape","$JPb"],
      '_$_gek':["getPreferredSizeWithContext","$rQb"],
      '_$_mgk':["onPropertyChanged","$rRb"],
      '_$_oqk':["updateContent","$ZVb"],
      '_$_xuk':["onItemFocusedChanged","$eYb"],
      '_$_gvk':["onItemSelectedChanged","$nYb"],
      '_$_zwk':["onItemHighlightedChanged","$bZb"],
      '_$_yal':["updateFor","$Qac"],
      '_$_pbl':["lookupContext","$gbc"],
      '_$_tbl':["initializeFrom","$kbc"],
      '_$_hvl':["TemplateStyleBase","QWB"],
      '_$_ivl':["GraphItemTemplateContextBase","RWB"],
      '_$_jvl':["MapperBasedUserTagProvider","TWB"],
      '_$_kvl':["ITemplateLabelStyle","UWB"],
      '_$_lvl':["TemplateLabelStyleRenderer","XWB"],
      '_$_mvl':["ITemplateNodeStyle","ZWB"],
      '_$_nvl':["TemplateNodeStyleRenderer","CXB"],
      '_$_ovl':["ITemplatePortStyle","EXB"],
      '_$_pvl':["TemplatePortStyleRenderer","GXB"],
      '_$$_ey':["item","$eg"],
      '_$$_lz':["canvas","$Ng"],
      '_$$_qz':["insets","$Qg"],
      '_$$_gab':["autoFlip","$mh"],
      '_$$_nab':["styleTag","$xh"],
      '_$$_oab':["template","$yh"],
      '_$$_yab':["renderSize","$Yh"],
      '_$$_ebb':["minimumSize","$ni"],
      '_$$_mbb':["outlineShape","$Gi"],
      '_$$_qbb':["preferredSize","$Qi"],
      '_$$_rcb':["setTag","$gm"],
      '_$$_teb':["content","$kE"],
      '_$$_mfb':["isFlipped","$dH"],
      '_$$_nfb':["labelText","$fH"],
      '_$$_hgb':["svgContent","$iJ"],
      '_$$_kgb':["itemFocused","$dK"],
      '_$$_tgb':["isUpsideDown","$AL"],
      '_$$_ugb':["itemSelected","$BL"],
      '_$$_mhb':["itemHighlighted","$LQ"],
      '_$$_rhb':["renderTemplateId","$ZS"],
      '_$$_blb':["setArrangeRect","$APb"],
      '_$$_dlb':["onPropertyChanged","$rRb"],
      '_$$_elb':["onLabelTextChanged","$TRb"],
      '_$$_flb':["updateContent","$ZVb"],
      '_$$_glb':["writeBackLabelText","$JXb"],
      '_$$_hlb':["updateFor","$Qac"],
      '_$$_ilb':["initializeFrom","$kbc"],
      '_$$_epb':["yfiles.drawing","yfiles._R"],
      '_$$_fpb':["IUIElementStyle","SWB"],
      '_$$_gpb':["TemplateLabelStyle","VWB"],
      '_$$_hpb':["StringTemplateLabelStyle","WWB"],
      '_$$_ipb':["LabelTemplateContext","YWB"],
      '_$$_jpb':["TemplateNodeStyle","AXB"],
      '_$$_kpb':["StringTemplateNodeStyle","BXB"],
      '_$$_lpb':["NodeTemplateContext","DXB"],
      '_$$_mpb':["TemplatePortStyle","FXB"],
      '_$$_npb':["StringTemplatePortStyle","HXB"],
      '_$$_opb':["PortTemplateContext","IXB"]
    },yfiles.mappings);
  }
  return undefined;
});
