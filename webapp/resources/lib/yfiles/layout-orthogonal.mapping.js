/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_uzc':["edgeLayoutDescriptor","$ak"],
      '_$_mse':["grid","$PC"],
      '_$_bdf':["nodeModel","$rH"],
      '_$_vif':["layoutStyle","$jK"],
      '_$_dpf':["layoutQuality","$kN"],
      '_$_mbg':["useRandomization","$bT"],
      '_$_nbg':["useSketchDrawing","$cT"],
      '_$_ifg':["usePostprocessing","$MU"],
      '_$_xfg':["considerNodeLabels","$aV"],
      '_$_yig':["useLengthReduction","$mW"],
      '_$_cjg':["alignDegreeOneNodes","$qW"],
      '_$_fmg':["useFaceMaximization","$OX"],
      '_$_kog':["minimumSegmentLength","$PY"],
      '_$_grg':["postprocessingEnabled","$iab"],
      '_$_wsg':["integratedEdgeLabeling","$Wab"],
      '_$_ntg':["optimizePerceivedBends","$gbb"],
      '_$_dug':["useSpacePostprocessing","$vbb"],
      '_$_qwg':["minimumLastSegmentLength","$Dcb"],
      '_$_hyg':["minimumFirstSegmentLength","$odb"],
      '_$_wyg':["useCrossingPostprocessing","$Cdb"],
      '_$_xfh':["createCopy","$Pgb"],
      '_$_tsj':["createEdgeLayoutDescriptor","$sLb"],
      '_$_aan':["DirectedOrthogonalLayouter","RED"],
      '_$_ban':["EdgeLayoutDescriptor","SED"],
      '_$_can':["LayoutStyle","TED"],
      '_$_dan':["OrthogonalGroupLayouter","UED"],
      '_$_ean':["OrthogonalLayouter","VED"],
      '_$$_wtb':["yfiles.orthogonal","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
