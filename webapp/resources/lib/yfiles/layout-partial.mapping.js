/****************************************************************************
 **
 ** This file is part of yFiles for HTML 1.2.0.1.
 ** 
 ** yWorks proprietary/confidential. Use is subject to license terms.
 **
 ** Copyright (c) 2014 by yWorks GmbH, Vor dem Kreuzberg 28, 
 ** 72070 Tuebingen, Germany. All rights reserved.
 **
 ***************************************************************************/
(typeof define=='function'?define:(function(dependencies, fn){fn();}))(['./lang'],function(){
  {
    if(!yfiles.mappings)yfiles.mappings={};
    {
      var myMappingsVersion="1.2.0.1-Complete Evaluation (Build 112d5d572e7f)";
      if(myMappingsVersion){
        if(yfiles.mappings&&yfiles.mappings.$version&&yfiles.mappings.$version!==myMappingsVersion){
          throw new Error("Mismatching library versions. Required: "+myMappingsVersion+", found: "+yfiles.mappings.$version);
        }else{
          yfiles.mappings.$version=myMappingsVersion;
        }
      }
    }
    yfiles.lang.copyOwnTo({
      '_$_sub':["StraightLineEdgeRouter","$a"],
      '_$_tub':["WithCoreLayouter","$b"],
      '_$_rzc':["minimalNodeDistance","$Yj"],
      '_$_iee':["routeInterEdges","$rw"],
      '_$_tef':["edgeRouter","$eI"],
      '_$_zqf':["allowMirroring","$eO"],
      '_$_euf':["packComponents","$DP"],
      '_$_twf':["maximalDuration","$RQ"],
      '_$_sdg':["layoutOrientation","$bU"],
      '_$_akg':["edgeRoutingStrategy","$NW"],
      '_$_dlg':["positioningStrategy","$lX"],
      '_$_dqg':["considerNodeAlignment","$HZ"],
      '_$_sxg':["fixedGroupResizingEnabled","$bdb"],
      '_$_myg':["optimizeLayoutOrientation","$sdb"],
      '_$_pzg':["routeInterEdgesImmediately","$Udb"],
      '_$_xzg':["componentAssignmentStrategy","$ceb"],
      '_$_zai':["doPartialLayout","$Pqb"],
      '_$_xak':["layoutSubgraph","$ZOb"],
      '_$_gik':["configureEdgeRouter","$gSb"],
      '_$_crk':["placeSubgraphs","$mWb"],
      '_$_uzk':["routeEdgesBetweenFixedElements","$nac"],
      '_$_gan':["EdgeRoutingStrategy","XED"],
      '_$_han':["ComponentAssignmentStrategy","YED"],
      '_$_ian':["SubgraphPositioningStrategy","ZED"],
      '_$_jan':["LayoutOrientation","AFD"],
      '_$_kan':["PartialLayouter","BFD"],
      '_$$_ytb':["yfiles.partial","yfiles._R"]
    },yfiles.mappings);
  }
  return undefined;
});
