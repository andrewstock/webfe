exports.config = {
  "watch": {
	  "sourceDir": "src/main/app",
	  "compiledDir": "webapp/resources"
  },
  "vendor": {
	"javascripts": "lib/js",
	"stylesheets": "lib/css"
  },
  "modules": [
    "copy",
  //  "jshint", 
    "csslint",
    "require",
    "minify-js",
    "minify-css",
    "bower",
    "less"
  ],
  "server": {
    "views": {
      "compileWith": "html",
      "extension": "html"
    }
  }
}